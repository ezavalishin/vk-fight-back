<?php


use Illuminate\Http\Request;
use SwooleTW\Http\Websocket\Facades\Websocket;

/*
|--------------------------------------------------------------------------
| Websocket Routes
|--------------------------------------------------------------------------
|
| Here is where you can register websocket events for your application.
|
*/

Websocket::on('connect', function (\SwooleTW\Http\Websocket\Websocket $websocket, Request $request) {
    $websocket->loginUsing(\Illuminate\Support\Facades\Auth::user());
});

Websocket::on('disconnect', function ($websocket) {
    // called while socket on disconnect
});

Websocket::on('example', function (\SwooleTW\Http\Websocket\Websocket $websocket, $data) {
    $websocket->emit('message', $data . ' - ' . $websocket->getUserId());
});


Websocket::on('create-online-game', 'App\Http\Controllers\Websocket\GameController@createOnlineGame');
Websocket::on('start-offline-game', 'App\Http\Controllers\Websocket\GameController@startOfflineGame');

Websocket::on('connect-to-online-game', 'App\Http\Controllers\Websocket\GameController@connetToOnlineGame');

Websocket::on('answer', 'App\Http\Controllers\Websocket\GameController@answer');

Websocket::on('react', 'App\Http\Controllers\Websocket\ReactController@sendReact');

Websocket::on('send-invite', 'App\Http\Controllers\Websocket\GameController@sendInvite');
Websocket::on('send-invite-by-game', 'App\Http\Controllers\Websocket\GameController@sendInviteByLastGameId');

Websocket::on('decline-invite', 'App\Http\Controllers\Websocket\GameController@declineInvite');

Websocket::on('get-active-game', 'App\Http\Controllers\Websocket\GameController@getActiveGame');
Websocket::on('get-active-question', 'App\Http\Controllers\Websocket\GameController@getActiveQuestion');
