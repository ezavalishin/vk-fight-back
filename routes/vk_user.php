<?php

/**
 * @var \Laravel\Lumen\Routing\Router $router
 */

$router->post('auth', 'MeController@auth');

$router->get('my-games', 'GameController@myGames');

$router->get('rating', 'UserController@rating');
$router->get('friends', 'UserController@getFriends');

$router->post('cors', 'CorsAnywhereController@post');
$router->post('error-log', 'ErrorLogController@store');
