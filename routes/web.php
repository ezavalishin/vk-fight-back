<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'a u here??';
});

$router->get('/docs', function() {
    return view('docs');
});

$router->get('test', function () {
    return view('test');
});

$router->post('auth', 'Api\AuthController@auth');
