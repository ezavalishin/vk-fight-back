<?php

/**
 * @var \Laravel\Lumen\Routing\Router $router
 */

$router->get('get-question', 'QuestionController@getFromParser');
$router->post('skip-question', 'QuestionController@skipDatabase');
$router->get('last-id', 'QuestionController@getDatabaseLastId');

$router->group(['prefix' => 'questions'], function () use ($router) {
    $router->get('/', 'QuestionController@index');
    $router->get('/search', 'QuestionController@search');
    $router->post('/filter', 'QuestionController@filter');
    $router->get('/{questionId}', 'QuestionController@getAnswers');
    $router->delete('/{questionId}', 'QuestionController@delete');
    $router->post('/', 'QuestionController@store');
    $router->put('/{questionId}', 'QuestionController@update');
    $router->post('/{questionId}/add-answer', 'QuestionController@addAnswer');
    $router->get('/{questionId}/tags', 'QuestionController@getTags');
    $router->post('/{questionId}/attach-tag', 'QuestionController@attachTag');
    $router->post('/{questionId}/detach-tag', 'QuestionController@detachTag');
});

$router->group(['prefix' => 'answers'], function () use ($router) {
    $router->delete('/{answerId}', 'AnswerController@delete');
    $router->put('/{answerId}', 'AnswerController@update');
});

$router->group(['prefix' => 'users'], function () use ($router) {
    $router->get('/', 'UserController@index');
});

$router->group(['prefix' => 'tags'], function () use ($router) {
    $router->get('/', 'TagController@index');
});

$router->get('stats', 'StatsController@index');
$router->get('me', 'MeController@me');
$router->get('error-logs', 'ErrorLogController@index');
