<?php

use Illuminate\Database\Seeder;

class EmotionsTableSeeder extends Seeder
{

    private function data()
    {
        return [
            [
                'slug' => 'default',
                'title' => 'Начало битвы',
                'emotions' => [
                    [
                        'title' => 'cool',
                        'filename' => 'cool.png'
                    ],
                    [
                        'title' => 'sweat',
                        'filename' => 'sweat.png'
                    ],
                    [
                        'title' => 'think',
                        'filename' => 'think.png'
                    ]
                ]
            ],
            [
                'slug' => 'admin',
                'title' => 'Админские',
                'emotions' => [
                    [
                        'title' => 'middle-finger',
                        'filename' => 'middle-finger.png'
                    ],
                    [
                        'title' => 'two-pink-hearths',
                        'filename' => 'two-pink-hearths.png'
                    ],
                    [
                        'title' => 'cock',
                        'filename' => 'cock.png'
                    ]
                ]
            ]
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data() as $itemPack) {
            $emotionPack = \App\EmotionPack::query()->firstOrCreate([
                'slug' => $itemPack['slug']
            ], [
                'title' => $itemPack['title'],
            ]);

            foreach ($itemPack['emotions'] as $item) {
                $emotionPack->emotions()->firstOrCreate([
                    'title' => $item['title'],
                ], [
                    'filename' => $item['filename']
                ]);
            }
        }
    }
}
