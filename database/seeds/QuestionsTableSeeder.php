<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = json_decode(file_get_contents(storage_path('app/seed/questions.json')), true);

        foreach ($content as $questionData) {

            $tags = (new \Illuminate\Support\Collection($questionData['tags']))->map(function ($tagTitle) {
                return \App\Tag::query()->firstOrCreate([
                    'title' => mb_strtolower($tagTitle)
                ]);
            });

            /**
             * @var \App\Question $question
             */
            $question = \App\Question::query()->create([
                'title' => $questionData['title'],
                'weight' => $questionData['weight']
            ]);

            $question->tags()->sync($tags->pluck('id'));

            foreach ($questionData['answers'] as $answerData) {
                $question->answers()->create([
                    'title' => $answerData['title'],
                    'is_right' => $answerData['isRight']
                ]);
            }
        }
    }
}
