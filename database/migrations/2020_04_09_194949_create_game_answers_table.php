<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_answers', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->on('users')->references('id')->onDelete('cascade');

            $table->unsignedBigInteger('game_question_id');
            $table->foreign('game_question_id')->on('game_questions')->references('id')->onDelete('cascade');

            $table->unsignedBigInteger('answer_id');
            $table->foreign('answer_id')->on('answers')->references('id')->onDelete('cascade');

            $table->timestamp('answered_at')->nullable();
            $table->boolean('is_first');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_answers');
    }
}
