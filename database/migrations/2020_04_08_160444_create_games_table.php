<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('creator_id');
            $table->foreign('creator_id')->on('users')->references('id')->onDelete('cascade');

            $table->unsignedBigInteger('opponent_id')->nullable();
            $table->foreign('opponent_id')->on('users')->references('id')->onDelete('cascade');

            $table->unsignedBigInteger('winner_id')->nullable();
            $table->foreign('winner_id')->on('users')->references('id')->onDelete('cascade');

            $table->timestamp('started_at')->nullable();
            $table->timestamp('finished_at')->nullable();

            $table->unsignedInteger('creator_points')->default(0);
            $table->unsignedInteger('opponent_points')->default(0);

            $table->boolean('is_online');
            $table->boolean('is_active')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
