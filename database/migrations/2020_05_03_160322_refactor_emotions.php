<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactorEmotions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('emotion_user');

        Schema::create('emotion_pack_user', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->on('users')->references('id')->onDelete('cascade');

            $table->unsignedBigInteger('emotion_pack_id');
            $table->foreign('emotion_pack_id')->on('emotion_packs')->references('id')->onDelete('cascade');

            $table->unique(['user_id', 'emotion_pack_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('emotion_user', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->on('users')->references('id')->onDelete('cascade');

            $table->unsignedBigInteger('emotion_id');
            $table->foreign('emotion_id')->on('emotions')->references('id')->onDelete('cascade');

            $table->unique(['user_id', 'emotion_id']);

            $table->timestamps();
        });

        Schema::dropIfExists('emotion_pack_user');
    }
}
