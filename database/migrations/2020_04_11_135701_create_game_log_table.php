<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_logs', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('game_id');
            $table->foreign('game_id')->on('games')->references('id')->onDelete('cascade');

            $table->unsignedBigInteger('user_one_id');
            $table->foreign('user_one_id')->on('users')->references('id')->onDelete('cascade');

            $table->unsignedBigInteger('user_two_id');
            $table->foreign('user_two_id')->on('users')->references('id')->onDelete('cascade');

            $table->unsignedInteger('user_one_points');
            $table->unsignedInteger('user_two_points');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_logs');
    }
}
