<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MultiplayerRefactoring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->dropColumn(['opponent_id', 'opponent_points', 'creator_points']);
        });

        Schema::table('game_questions', function (Blueprint $table) {
            $table->dropColumn(['creator_points', 'opponent_points']);
        });

        Schema::create('game_users', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->on('users')->references('id')->onDelete('cascade');

            $table->unsignedBigInteger('game_id');
            $table->foreign('game_id')->on('games')->references('id')->onDelete('cascade');

            $table->unsignedInteger('points')->default(0);

            $table->timestamp('started_at')->nullable();
            $table->timestamp('finished_at')->nullable();

            $table->timestamp('last_question_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->unsignedBigInteger('opponent_id')->nullable();
            $table->foreign('opponent_id')->on('users')->references('id')->onDelete('cascade');

            $table->unsignedInteger('creator_points')->default(0);
            $table->unsignedInteger('opponent_points')->default(0);
        });

        Schema::table('game_questions', function (Blueprint $table) {
            $table->unsignedInteger('creator_points')->default(0);
            $table->unsignedInteger('opponent_points')->default(0);
        });

        Schema::dropIfExists('game_users');
    }
}
