<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPointsToGameQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_questions', function (Blueprint $table) {
            $table->unsignedInteger('creator_points')->default(0);
            $table->unsignedInteger('opponent_points')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_questions', function (Blueprint $table) {
            $table->dropColumn(['creator_points', 'opponent_points']);
        });
    }
}
