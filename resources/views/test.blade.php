<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script>
    !function (t, i) {
        "object" == typeof exports && "undefined" != typeof module ? module.exports = i() : "function" == typeof define && define.amd ? define(i) : (t = t || self).VueCountdown = i()
    }(this, function () {
        "use strict";
        var t = 1e3, i = 6e4, s = 36e5, e = "visibilitychange";
        return {
            name: "countdown",
            data: function () {
                return {counting: !1, endTime: 0, totalMilliseconds: 0}
            },
            props: {
                autoStart: {type: Boolean, default: !0},
                emitEvents: {type: Boolean, default: !0},
                interval: {
                    type: Number, default: 1e3, validator: function (t) {
                        return 0 <= t
                    }
                },
                now: {
                    type: Function, default: function () {
                        return Date.now()
                    }
                },
                tag: {type: String, default: "span"},
                time: {
                    type: Number, default: 0, validator: function (t) {
                        return 0 <= t
                    }
                },
                transform: {
                    type: Function, default: function (t) {
                        return t
                    }
                }
            },
            computed: {
                days: function () {
                    return Math.floor(this.totalMilliseconds / 864e5)
                }, hours: function () {
                    return Math.floor(this.totalMilliseconds % 864e5 / s)
                }, minutes: function () {
                    return Math.floor(this.totalMilliseconds % s / i)
                }, seconds: function () {
                    return Math.floor(this.totalMilliseconds % i / t)
                }, milliseconds: function () {
                    return Math.floor(this.totalMilliseconds % t)
                }, totalDays: function () {
                    return this.days
                }, totalHours: function () {
                    return Math.floor(this.totalMilliseconds / s)
                }, totalMinutes: function () {
                    return Math.floor(this.totalMilliseconds / i)
                }, totalSeconds: function () {
                    return Math.floor(this.totalMilliseconds / t)
                }
            },
            render: function (t) {
                return t(this.tag, this.$scopedSlots.default ? [this.$scopedSlots.default(this.transform({
                    days: this.days,
                    hours: this.hours,
                    minutes: this.minutes,
                    seconds: this.seconds,
                    milliseconds: this.milliseconds,
                    totalDays: this.totalDays,
                    totalHours: this.totalHours,
                    totalMinutes: this.totalMinutes,
                    totalSeconds: this.totalSeconds,
                    totalMilliseconds: this.totalMilliseconds
                }))] : this.$slots.default)
            },
            watch: {
                $props: {
                    deep: !0, immediate: !0, handler: function () {
                        this.totalMilliseconds = this.time, this.endTime = this.now() + this.time, this.autoStart && this.start()
                    }
                }
            },
            methods: {
                start: function () {
                    this.counting || (this.counting = !0, this.emitEvents && this.$emit("start"), "visible" === document.visibilityState && this.continue())
                }, continue: function () {
                    var e = this;
                    if (this.counting) {
                        var n = Math.min(this.totalMilliseconds, this.interval);
                        if (0 < n) if (window.requestAnimationFrame) {
                            var o, a;
                            this.requestId = requestAnimationFrame(function t(i) {
                                a = a || i;
                                var s = i - (o = o || i);
                                n <= s || n <= s + (i - a) / 2 ? e.progress() : e.requestId = requestAnimationFrame(t), a = i
                            })
                        } else this.timeoutId = setTimeout(function () {
                            e.progress()
                        }, n); else this.end()
                    }
                }, pause: function () {
                    window.requestAnimationFrame ? cancelAnimationFrame(this.requestId) : clearTimeout(this.timeoutId)
                }, progress: function () {
                    this.counting && (this.totalMilliseconds -= this.interval, this.emitEvents && 0 < this.totalMilliseconds && this.$emit("progress", {
                        days: this.days,
                        hours: this.hours,
                        minutes: this.minutes,
                        seconds: this.seconds,
                        milliseconds: this.milliseconds,
                        totalDays: this.totalDays,
                        totalHours: this.totalHours,
                        totalMinutes: this.totalMinutes,
                        totalSeconds: this.totalSeconds,
                        totalMilliseconds: this.totalMilliseconds
                    }), this.continue())
                }, abort: function () {
                    this.counting && (this.pause(), this.counting = !1, this.emitEvents && this.$emit("abort"))
                }, end: function () {
                    this.counting && (this.pause(), this.totalMilliseconds = 0, this.counting = !1, this.emitEvents && this.$emit("end"))
                }, update: function () {
                    this.counting && (this.totalMilliseconds = Math.max(0, this.endTime - this.now()))
                }, handleVisibilityChange: function () {
                    switch (document.visibilityState) {
                        case"visible":
                            this.update(), this.continue();
                            break;
                        case"hidden":
                            this.pause()
                    }
                }
            },
            mounted: function () {
                document.addEventListener(e, this.handleVisibilityChange)
            },
            beforeDestroy: function () {
                document.removeEventListener(e, this.handleVisibilityChange), this.pause()
            }
        }
    });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.3.0/socket.io.js"></script>


    <style>
    .btn.is-correct {
        background-color: green !important;
    }

    .btn.is-selected {
        background-color: black !important;
    }

    .relative {
        position: relative;
    }

    .relative .avatar-block {
        position: absolute;
        top: 50%;
        transform: translate3d(0, -50%, 0);
        right: -12px;
        display: flex;
    }

    .relative img {
        width: 24px;
        height: 24px;
        border-radius: 100%;
        margin: 2px
    }

    .game-bar {
        position: fixed;
        bottom: 30px;
        height: 60px;
        left: 30px;
        right: 30px;
    }

    .game-bar .user {
        position: absolute;
        top: 0;
    }

    .game-bar .avatar {
        width: 60px;
        height: 60px;
        border-radius: 100%;
    }

    .game-bar .creator {
        left: 0;
    }

    .game-bar .opponent {
        right: 0;
    }

    .progress-bar {
        position: absolute;
        right: 100px;
        left: 100px;
        top: 50%;
        background-color: red;
        height: 10px;
    }

    .inner-bar {
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        transition: .5s width;
        background-color: green;
    }

    img {
        border-radius: 100%;
    }

    .emojis {
        display: flex;
        justify-content: space-around;
        align-items: center;
        font-size: 70px;

        position: absolute;
        bottom: 50px;
        left: 0;
        right: 0;
    }

    .particle {
        position: absolute;
    }

    #canvas {
        position: absolute;
        top: 0;
        left: 0;
        pointer-events: none;
        z-index: 100;
    }
    </style>
</head>
<body>

<div id="app">
    <img id="test" src="http://vk-fight-back.test/storage/emotions/sweat.png" alt="">
    <canvas id="canvas"></canvas>
    <div class="container">
        <div class="input-field" v-if="!loggedIn">
            <input v-model="vkParams" placeholder="Vk Params" type="text">
            <button class="btn" @click="login">login</button>
        </div>

        <div class="row">
            <div class="col s6" v-if="loggedIn">
                <div class="input-field">
                    <input v-model="gameId" placeholder="Game Id" type="text">
                </div>

                <div class="mt-5">
                    <button @click="createGame" class="btn">Create game</button>
                </div>

                <div class="mt-5">
                    <button @click="joinGame" class="btn">Join game</button>
                </div>

                <div class="mt-5">
                    <button @click="startOffline" class="btn">Start offline</button>
                </div>

                <div class="mt-5">
                    <button @click="sendInvite(1,2)" class="btn">Invite Test</button>
                </div>

                <div class="mt-5">
                    <button @click="declineInvite(1)" class="btn">Decline invite Test</button>
                </div>

                <div class="mt-5">
                    <button @click="getActiveGame" class="btn">Get active question</button>
                </div>

                <div class="mt-5">
                    <button @click="getActiveQuestion" class="btn">Get active question</button>
                </div>
            </div>

            <h1 v-if="timer" class="col s6">
                <countdown :time="timer">
                    <template slot-scope="props">@{{ props.seconds }}</template>
                </countdown>
            </h1>
        </div>


        <div v-if="screen === 'preview'">
            <div class="row">
                <div class="col s6 center-align">
                    <img :src="game.creator.avatar" alt="">
                    <h2>@{{ game.creator.first_name }}</h2>
                </div>
                <div class="col s6 center-align" v-if="game.opponent">
                    <img :src="game.opponent.avatar" alt="">
                    <h2>@{{ game.opponent.first_name }}</h2>
                </div>
            </div>
        </div>

        <div v-if="screen === 'question'">
            <h2>@{{ question.title }}</h2>

            <div class="center-align" v-for="answer in question.answers" :key="answer.id" style="margin: 10px">
                <button
                    :class="{'is-correct': answer.id == correctAnswerId, 'is-selected': answer.id == selectedAnswerId}"
                    @click="answerAQuestion(answer.id)" :disabled="questionIsClosed" style="width: 300px"
                    class="btn relative">
                    @{{ answer.title }}
                    <div class="avatar-block" v-if="users[answer.id]">
                        <img v-for="user in users[answer.id]" :key="user.id" :src="user.avatar" alt="">
                    </div>
                </button>
            </div>
        </div>

        <div v-if="screen === 'end'">
            <div class="center-align" v-if="!winnerId">
                <h1>НИЧЬЯ</h1>
            </div>
            <div class="row">
                <div class="col s6 center-align">
                    <img :src="creator.avatar" alt="">
                    <h2>@{{ creator.first_name }}</h2>
                    <h3>@{{ creator.points }}</h3>
                    <h3 v-if="winnerId == creator.id">Winner!</h3>
                </div>
                <div class="col s6 center-align" v-if="game.opponent">
                    <img :src="opponent.avatar" alt="">
                    <h2>@{{ opponent.first_name }}</h2>
                    <h3>@{{ opponent.points }}</h3>
                    <h3 v-if="winnerId == opponent.id">Winner!</h3>
                </div>
            </div>
        </div>

        <div v-if="game" class="game-bar">

            <div v-if="emoji" class="emoji-fountain">
                <div>@{{emoji}}</div>
            </div>
            <div class="emojis">
                <div @click="sendEmoji(1)">
                    😀
                </div>
                <div @click="sendEmoji(2)">
                    😡
                </div>
                <div @click="sendEmoji(3)">
                    🖕
                </div>
            </div>

            <div class="progress-bar">
                <div class="inner-bar" :style="{width: progressWidth + '%'}"></div>
            </div>

            <div class="user creator">
                <img class="avatar creator" :src="creator.avatar" alt="">
                <strong>@{{ creator.points }}</strong>
            </div>

            <div class="user opponent" v-if="game.opponent">
                <img class="avatar opponent" :src="opponent.avatar" alt="">
                <strong>@{{ opponent.points }}</strong>
            </div>
        </div>
    </div>
</div>

<script>


</script>


<script>

const emojiCount = 10;

Vue.component(VueCountdown.name, VueCountdown);


new Vue({
    el: '#app',
    data: {
        loggedIn: false,
        vkParams: "eyJ2a19hY2Nlc3NfdG9rZW5fc2V0dGluZ3MiOiJtZW51IiwidmtfYXBwX2lkIjoiNzAyMDQwOSIsInZrX2FyZV9ub3RpZmljYXRpb25zX2VuYWJsZWQiOiIwIiwidmtfaXNfYXBwX3VzZXIiOiIxIiwidmtfaXNfZmF2b3JpdGUiOiIxIiwidmtfbGFuZ3VhZ2UiOiJydSIsInZrX3BsYXRmb3JtIjoiZGVza3RvcF93ZWIiLCJ2a19yZWYiOiJvdGhlciIsInZrX3VzZXJfaWQiOiIxMzE2MjgwMyIsInNpZ24iOiJGai1KeG5WVzV3WDd4R1dyZGFtdDB3NHAyYWZFVHF4eUxDbkk3YW8tbUNzIiwidXRjX29mZnNldCI6MjQwfQ==",
        gameId: null,
        socket: null,
        screen: null,

        game: null,

        question: null,
        questionIsClosed: false,

        correctAnswerId: null,
        selectedAnswerId: null,

        users: {},

        creator: null,
        opponent: null,

        progressWidth: 50,

        timer: null,

        winnerId: null,

        emoji: null,

        particles: [],
        looped: false,

        ctx: null,

        images: {}
    },

    mounted() {
        this.canvas = document.getElementById("canvas");
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.ctx = this.canvas.getContext('2d');

        const images = [
            'http://vk-fight-back.test/storage/emotions/sweat.png',
            'http://vk-fight-back.test/storage/emotions/cool.png',
            'http://vk-fight-back.test/storage/emotions/think.png',
        ];

        for(let i = 0; i<images.length; i++) {
            this.images[images[i]] = new Image();
            this.images[images[i]].src = images[i];
        }
    },

    methods: {
        login() {

                @if (env('APP_ENV') === 'production')
            const url = 'wss://{{env('APP_HOST')}}?vk-params=' + this.vkParams;
                @else
            const url = 'ws://{{env('APP_HOST')}}:1215?vk-params=' + this.vkParams;
            @endif

                this.socket = io(url, {
                transports: ['websocket'],
            });


            // this.socket.emit('get-active-game');
            //
            // this.socket.on('active-game', (msg) => {
            //
            //     if (msg.game_id) {
            //         this.socket.emit('connect-to-online-game', {
            //             'game_id': msg.game_id
            //         });
            //     }
            // });

            this.socket.on('game-created', (msg) => {
                this.gameId = msg.id;
            });


            this.socket.on('game-preview', (data) => {
                this.screen = 'preview';
                this.game = data;

                this.creator = this.game.creator;
                this.creator.points = 0;

                if (this.game.opponent) {
                    this.opponent = this.game.opponent;
                    this.opponent.points = 0;
                }

                if (data.ttw) {
                    this.timer = data.ttw * 1000;
                }
            });

            this.socket.on('new-question', (data) => {
                this.timer = null;
                this.questionIsClosed = false;
                this.screen = 'question';
                this.question = data;

                if (data.ttw) {
                    this.timer = data.ttw * 1000;
                }
            });

            this.socket.on('question-closed', (data) => {
                this.timer = null;
                this.questionIsClosed = true;

                this.selectedAnswerId = null;
                this.correctAnswerId = data.correct_answer_id;

                data.game_answers.forEach((gameAnswer) => {
                    const answerId = gameAnswer.answer_id;

                    if (!this.users[answerId]) {
                        this.users[answerId] = [];
                    }

                    this.users[answerId].push(gameAnswer.user);
                });

                this.creator.points += parseInt(data.points.creator, 10);

                if (this.opponent) {
                    this.opponent.points += parseInt(data.points.opponent, 10);
                }

                this.updateProgressWidth();


                if (data.ttw) {
                    this.timer = data.ttw * 1000;
                }
            });

            this.socket.on('game-closed', (data) => {
                this.winnerId = data.winner_id;
                this.screen = 'end';
                this.timer = null;
            });

            this.socket.on('badge', (data) => {
                M.toast({html: data.message});
            });

            this.socket.on('got-react', (data) => {
                this.runFountain(data.emotion_url);
            });

            this.loggedIn = true;
        },
        createGame() {
            this.socket.emit('create-online-game');
        },
        startOffline() {
            this.socket.emit('start-offline-game');
        },
        joinGame() {
            this.socket.emit('connect-to-online-game', {
                'game_id': this.gameId
            });
        },

        answerAQuestion(answerId) {
            this.socket.emit('answer', {
                'game_id': this.game.id,
                'answer_id': answerId
            });
            this.questionIsClosed = true;
            this.selectedAnswerId = answerId;
        },

        updateProgressWidth() {

            if (!this.game.opponent) {
                return 100;
            }

            const totalPoints = this.creator.points + this.opponent.points;

            if (totalPoints === 0) {
                return 50;
            }

            this.progressWidth = this.creator.points / totalPoints * 100;
        },
        sendEmoji(emotionId) {
            this.socket.emit('react', {
                'emotion_id': emotionId,
                'game_id': this.game.id
            })
        },

        getActiveQuestion() {
            this.socket.emit('get-active-question')
        },

        getActiveGame() {
            this.socket.emit('get-active-game')
        },

        runFountain(emoji) {
            this.emoji = emoji;

            for (let i = 0; i < emojiCount; i++) {
                this.createParticle(this.emoji);
            }


            if (!this.looped) {
                this.loop();
            }
        },

        loop() {
            if (this.particles.length === 0) {
                this.looped = false;
                return;
            }
            this.looped = true;

            this.updateParticles();

            requestAnimationFrame(this.loop);
        },

        createParticle(emoji) {
            const size = 50;
            const speedHorz = Math.random() * 10;
            const speedUp = Math.random() * 25;
            const spinVal = Math.random() * 360;
            const spinSpeed = ((Math.random() * 35)) * (Math.random() <= .5 ? -1 : 1);
            const top = 0;
            const left = 0;
            const direction = Math.random() <= .5 ? -1 : 1;

            const image = this.images[emoji];

            this.ctx.drawImage(image, left, top);


    //         particle.setAttribute("style", `
    //         width: 20px;
    //         height: auto;
    //   font-size: ${size}px;
    //   top: ${top}px;
    //   left: ${left}px;
    //   transform: rotate(${spinVal}deg);
    // `);
    //
    //         document.body.appendChild(particle);

            this.particles.push({
                image,
                size,
                speedHorz,
                speedUp,
                spinVal,
                spinSpeed,
                top,
                left,
                direction,
            });
        },

        updateParticles() {

            const height = this.canvas.height;
            this.ctx.clearRect(0,0,this.canvas.width, this.canvas.height);

            console.log(this.particles.length);


            this.particles.forEach((p) => {
                this.ctx.save();

                p.spinVal = p.spinVal + p.spinSpeed;
                p.left = p.left - (p.speedHorz * p.direction);
                p.top = p.top - p.speedUp;
                p.speedUp = Math.min(p.size, p.speedUp - 1);


                this.ctx.translate(p.left, p.top);
                this.ctx.rotate(p.spinVal * Math.PI / 180);
                this.ctx.translate((-p.image.width)/2, (-p.image.height)/2);


                this.ctx.drawImage(p.image, 0, 0);

                this.ctx.restore();


                if (p.top >= height + Math.max(p.image.height, p.image.width)) {
                    this.particles = this.particles.filter((o) => o !== p);
                    this.particles.slice(p, 1);
                }
            });
        },

        sendInvite(gameId, opponentId) {
            this.socket.emit('send-invite', {
                game_id: gameId,
                user_id: opponentId
            });
        },

        declineInvite(gameId) {
            this.socket.emit('decline-invite', {
                game_id: gameId
            });
        }
    },

    created() {

    }
})
</script>
</body>
</html>
