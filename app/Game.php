<?php

namespace App;

use App\Traits\Uuidable;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;


/**
 * App\Game
 *
 * @property int $id
 * @property int $creator_id
 * @property int|null $winner_id
 * @property Carbon|null $started_at
 * @property Carbon|null $finished_at
 * @property bool $is_online
 * @property bool $is_active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $creator
 * @property-read Collection|GameQuestion[] $gameQuestions
 * @property-read int|null $game_questions_count
 * @method static Builder|Game newModelQuery()
 * @method static Builder|Game newQuery()
 * @method static Builder|Game query()
 * @method static Builder|Game whereCreatedAt($value)
 * @method static Builder|Game whereCreatorId($value)
 * @method static Builder|Game whereFinishedAt($value)
 * @method static Builder|Game whereId($value)
 * @method static Builder|Game whereIsActive($value)
 * @method static Builder|Game whereIsOnline($value)
 * @method static Builder|Game whereStartedAt($value)
 * @method static Builder|Game whereUpdatedAt($value)
 * @method static Builder|Game whereWinnerId($value)
 * @mixin Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\GameUser[] $gameUsers
 * @property-read int|null $game_users_count
 * @property string|null $uuid
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Game whereUuid($value)
 */
class Game extends Model
{
    use Uuidable;

    protected $fillable = [
        'creator_id',
        'winner_id',
        'started_at',
        'finished_at',
        'is_active',
        'is_online'
    ];

    protected $dates = [
        'started_at',
        'finished_at',
    ];

    public function gameQuestions(): HasMany
    {
        return $this->hasMany(GameQuestion::class);
    }

    public function gameUsers(): HasMany
    {
        return $this->hasMany(GameUser::class);
    }

    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id');
    }


    public function hasOpponents() {

    }
}
