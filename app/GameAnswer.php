<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;


/**
 * App\GameAnswer
 *
 * @property-read Answer $answer
 * @property-read GameQuestion $gameQuestion
 * @property-read User $user
 * @method static Builder|GameAnswer newModelQuery()
 * @method static Builder|GameAnswer newQuery()
 * @method static Builder|GameAnswer query()
 * @mixin Eloquent
 * @property int $id
 * @property int $user_id
 * @property int $game_question_id
 * @property int $answer_id
 * @property string|null $answered_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|GameAnswer whereAnswerId($value)
 * @method static Builder|GameAnswer whereAnsweredAt($value)
 * @method static Builder|GameAnswer whereCreatedAt($value)
 * @method static Builder|GameAnswer whereGameQuestionId($value)
 * @method static Builder|GameAnswer whereId($value)
 * @method static Builder|GameAnswer whereUpdatedAt($value)
 * @method static Builder|GameAnswer whereUserId($value)
 * @property bool $is_first
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GameAnswer whereIsFirst($value)
 */
class GameAnswer extends Model
{
    protected $fillable = [
        'game_question_id',
        'answer_id',
        'user_id',
        'answered_at',
        'is_first'
    ];

    public function gameQuestion(): BelongsTo
    {
        return $this->belongsTo(GameQuestion::class);
    }

    public function answer(): BelongsTo
    {
        return $this->belongsTo(Answer::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
