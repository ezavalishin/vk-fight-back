<?php

namespace App\Listeners;

use App\Events\Contracts\HasUserContract;
use App\Services\VkClient;
use Exception;
use Illuminate\Support\Carbon;
use Spatie\Regex\Regex;

class FillUserInfo
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param HasUserContract $event
     * @return void
     * @throws Exception
     */
    public function handle(HasUserContract $event)
    {
        $user = $event->user();

        try {
            $data = (new VkClient())->getUsers($user->vk_user_id, [
                'first_name',
                'last_name',
                'photo_200',
                'bdate',
                'sex'
            ]);
        } catch (Exception $e) {
            return;
        }

        $user->first_name = $data['first_name'] ?? null;
        $user->last_name = $data['last_name'] ?? null;
        $user->avatar_200 = $data['photo_200'] ?? null;
        $user->sex = $data['sex'] ?? 0;

        if (isset($data['bdate'])) {
            $reYear = Regex::match('/\d{1,2}.\d{1,2}.\d{4}/', $data['bdate']);
            $reDay = Regex::match('/\d{1,2}.\d{1,2}/', $data['bdate']);

            if ($reYear->hasMatch()) {
                $user->bdate = Carbon::parse($data['bdate']);
            } elseif ($reDay->hasMatch()) {

                $date = explode('.', $data['bdate']);

                $bdate = new Carbon();

                $bdate->setYear(1);
                $bdate->setMonth($date[1]);
                $bdate->setDay($date[0]);

                $user->bdate = $bdate;

            } else {
                $user->bdate = null;
            }
        }

        $user->save();
    }
}
