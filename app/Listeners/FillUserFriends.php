<?php

namespace App\Listeners;

use App\Events\Contracts\HasUserContract;
use App\Services\VkClient;
use App\User;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class FillUserFriends
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param HasUserContract $event
     * @return void
     * @throws Exception
     */
    public function handle(HasUserContract $event)
    {
        try {
            $user = $event->user();

            $friends = (new VkClient())->getFriends($user->vk_user_id, [
                'id'
            ]);

            $friendIds = (new Collection($friends))->pluck('id');

            $userExistIds = User::query()->whereIn('vk_user_id', $friendIds)->select('id')->pluck('id');

            $oldFriendIds = $user->friends()->select('friend_id')->get()->pluck('friend_id');
            $user->friends()->sync($userExistIds);
            $newFriendIds = $user->friends()->select('friend_id')->get()->pluck('friend_id');

            $diffIds = $newFriendIds->diff($oldFriendIds);

            $diffIds->each(function ($id) use ($user) {
                try {
                    DB::table('friends')->insert([
                        'user_id' => $id,
                        'friend_id' => $user->id
                    ]);
                } catch (Exception $e) {

                }
            });
        } catch (Exception $e) {

        }

    }
}
