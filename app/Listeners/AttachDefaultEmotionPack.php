<?php

namespace App\Listeners;

use App\Emotion;
use App\EmotionPack;
use App\Events\Contracts\HasUserContract;
use App\Events\ExampleEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AttachDefaultEmotionPack
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ExampleEvent  $event
     * @return void
     */
    public function handle(HasUserContract $event)
    {
        $user = $event->user();

        $emotionPack = EmotionPack::whereSlug('default')->first();

        if (!$emotionPack) {
            return;
        }

        $user->emotionPacks()->attach($emotionPack);
    }
}
