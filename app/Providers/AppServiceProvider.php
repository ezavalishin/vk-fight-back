<?php

namespace App\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Fruitcake\Cors\CorsServiceProvider;
use Illuminate\Redis\RedisServiceProvider;
use Illuminate\Support\ServiceProvider;
use Laravel\Tinker\TinkerServiceProvider;
use Lorisleiva\LaravelDeployer\LaravelDeployerServiceProvider;
use SwooleTW\Http\LumenServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
        }

        $this->app->register(LumenServiceProvider::class);
        $this->app->register(TinkerServiceProvider::class);
        $this->app->register(RedisServiceProvider::class);
        $this->app->register(LaravelDeployerServiceProvider::class);
        $this->app->register(CorsServiceProvider::class);
    }
}
