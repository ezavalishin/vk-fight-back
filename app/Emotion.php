<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;


/**
 * App\Emotion
 *
 * @property int $id
 * @property string $title
 * @property string $filename
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Emotion newModelQuery()
 * @method static Builder|Emotion newQuery()
 * @method static Builder|Emotion query()
 * @method static Builder|Emotion whereCreatedAt($value)
 * @method static Builder|Emotion whereFilename($value)
 * @method static Builder|Emotion whereId($value)
 * @method static Builder|Emotion whereTitle($value)
 * @method static Builder|Emotion whereUpdatedAt($value)
 * @mixin Eloquent
 * @property int $emotion_pack_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Emotion whereEmotionPackId($value)
 * @property-read \App\EmotionPack $emotionPack
 */
class Emotion extends Model
{
    protected $fillable = [
        'title',
        'filename'
    ];

    public function getLink()
    {
        return Storage::disk('public')->url('emotions/' . $this->filename);
    }

    public function emotionPack()
    {
        return $this->belongsTo(EmotionPack::class);
    }
}
