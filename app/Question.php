<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;


/**
 * App\Question
 *
 * @property int $id
 * @property string $title
 * @property int $weight
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Answer[] $answers
 * @property-read int|null $answers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Tag[] $tags
 * @property-read int|null $tags_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereWeight($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\GameQuestion[] $gameQuestions
 * @property-read int|null $game_questions_count
 * @property float|null $right_answered_ratio
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Question whereRightAnsweredRatio($value)
 */
class Question extends Model
{
    public const EASY = 1;
    public const MEDIUM = 2;
    public const HARD = 3;

    protected $fillable = [
        'title',
        'weight',
    ];

    protected $casts = [
        'weight' => 'integer'
    ];

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }

    public function answers(): HasMany
    {
        return $this->hasMany(Answer::class);
    }

    public function gameQuestions(): HasMany
    {
        return $this->hasMany(GameQuestion::class);
    }


    public function updateRightAnswerRatio()
    {
        $rightAnswer = $this->answers()->where('is_right', true)->first();

        if (!$rightAnswer) {
            return;
        }

        $rightAnswerCount = $rightAnswer->loadCount('gameAnswers')->game_answers_count;

        $gameQuestions = $this->gameQuestions()->whereNotNull('started_at')->with('game')->get();

        $totalQuestionInGameCount = $gameQuestions->reduce(function ($carry, GameQuestion $gameQuestion) {
            return $carry + $gameQuestion->game->gameUsers->count();
        }, 0);

        if (!$totalQuestionInGameCount) {
            $this->right_answered_ratio = null;
        } else {
            $this->right_answered_ratio = $rightAnswerCount / $totalQuestionInGameCount;
        }
        $this->save();
    }
}
