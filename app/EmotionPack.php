<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


/**
 * App\EmotionPack
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Emotion[] $emotions
 * @property-read int|null $emotions_count
 * @method static Builder|EmotionPack newModelQuery()
 * @method static Builder|EmotionPack newQuery()
 * @method static Builder|EmotionPack query()
 * @method static Builder|EmotionPack whereCreatedAt($value)
 * @method static Builder|EmotionPack whereId($value)
 * @method static Builder|EmotionPack whereSlug($value)
 * @method static Builder|EmotionPack whereTitle($value)
 * @method static Builder|EmotionPack whereUpdatedAt($value)
 * @mixin Eloquent
 */
class EmotionPack extends Model
{
    protected $fillable = [
        'title',
        'slug'
    ];

    public function emotions()
    {
        return $this->hasMany(Emotion::class);
    }
}
