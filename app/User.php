<?php

namespace App;

use App\Events\UserCreated;
use Eloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Laravel\Lumen\Auth\Authorizable;

/**
 * App\User
 *
 * @property int $id
 * @property bool $notifications_are_enabled
 * @property bool $messages_are_enabled
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $avatar_200
 * @property string|null $bdate
 * @property int $sex
 * @property int|null $utc_offset
 * @property string|null $visited_at
 * @property bool $is_admin
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereAvatar200($value)
 * @method static Builder|User whereBdate($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereFirstName($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereIsAdmin($value)
 * @method static Builder|User whereLastName($value)
 * @method static Builder|User whereMessagesAreEnabled($value)
 * @method static Builder|User whereNotificationsAreEnabled($value)
 * @method static Builder|User whereSex($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static Builder|User whereUtcOffset($value)
 * @method static Builder|User whereVisitedAt($value)
 * @property-read Collection|User[] $friends
 * @property-read int|null $friends_count
 * @mixin Eloquent
 * @property int $rating
 * @method static Builder|User whereRating($value)
 * @property int|null $vk_user_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\GameUser[] $gameUsers
 * @property-read int|null $game_users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereVkUserId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\GameAnswer[] $gameAnswers
 * @property-read int|null $game_answers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\GameAnswer[] $rightGameAnswers
 * @property-read int|null $right_game_answers_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\EmotionPack[] $emotionPacks
 * @property-read int|null $emotion_packs_count
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $fillable = [
        'vk_user_id',
        'notifications_are_enabled',
        'messages_are_enabled',
        'first_name',
        'last_name',
        'avatar_200',
        'bdate',
        'sex',
        'utc_offset',
        'visited_at'
    ];

    protected $casts = [
        'id' => 'integer',
        'notifications_are_enabled' => 'boolean',
        'messages_are_enabled' => 'boolean',
    ];

    protected $dates = [
        'visited_at'
    ];

    protected $attributes = [
        'rating' => 0
    ];

    protected $dispatchesEvents = [
        'created' => UserCreated::class
    ];

    public function friends()
    {
        return $this->belongsToMany(self::class, 'friends', 'user_id', 'friend_id');
    }

    public function gameUsers()
    {
        return $this->hasMany(GameUser::class);
    }

    public function gameAnswers()
    {
        return $this->hasMany(GameAnswer::class);
    }

    public function emotionPacks()
    {
        return $this->belongsToMany(EmotionPack::class)->withTimestamps();
    }

    public function rightGameAnswers()
    {
        return $this->hasMany(GameAnswer::class)->whereHas('answer', function (Builder $q) {
            return $q->where('is_right', true);
        });
    }

    public static function byVkId(int $id)
    {
        return self::query()->firstOrCreate([
            'vk_user_id' => $id
        ]);
    }

    public function roomChannel($gameId)
    {
        return 'offline' . $gameId . '_' . $this->id;
    }

    public function updateRating()
    {
        $this->rating = $this->gameUsers()->sum('points');
        $this->save();
    }

    public function hasActiveGame()
    {
        return $this->gameUsers()->whereHas('game', function ($q) {
            return $q->where('is_active', true);
        })->exists();
    }
}
