<?php


namespace App\Http\Resources;

use App\Game;
use App\Question;
use App\Services\GameService;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin Question
 */
class QuestionResource extends JsonResource
{
    public function toArray($request)
    {
        if ($this->getAttribute('order')) {
            $questionOrder = $this->getAttribute('order');
        } else {
            $questionOrder = null;
        }

        return [
            'id' => $this->id,
            'title' => $this->title,
            'answers' => AnswerResource::collection($this->whenLoaded('answers')),
            'ttw' => GameService::QUESTION_TO_ANSWER,
            'order' => $questionOrder
        ];
    }
}
