<?php


namespace App\Http\Resources;

use App\Answer;
use App\GameAnswer;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin GameAnswer
 */
class GameAnswerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'answer_id' => $this->answer_id,
            'user' => new UserResource($this->user)
        ];
    }
}
