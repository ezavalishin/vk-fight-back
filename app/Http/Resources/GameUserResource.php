<?php


namespace App\Http\Resources;

use App\Game;
use App\GameUser;
use App\Services\GameService;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin GameUser|User
 */
class GameUserResource extends JsonResource
{
    public function toArray($request)
    {
        return (new UserResource($this->user))->toArray($request);
    }
}
