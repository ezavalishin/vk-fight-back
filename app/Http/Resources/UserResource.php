<?php


namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin User
 */
class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'vk_user_id' => $this->vk_user_id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'avatar' => $this->avatar_200,
            'rating' => $this->rating,
            'games_count' => $this->gamesCount ?? new MissingValue(),
            'wins_count' => $this->winsCount ?? new MissingValue(),
            'global_place' => $this->globalPlace ?? new MissingValue(),
            'friend_place' => $this->friendPlace ?? new MissingValue(),
            'friends' => UserResource::collection($this->whenLoaded('friends')),
            'emotionPacks' => EmotionPackResource::collection($this->whenLoaded('emotionPacks'))
        ];
    }
}
