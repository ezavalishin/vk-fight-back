<?php


namespace App\Http\Resources;

use App\Game;
use App\GameUser;
use App\Services\GameService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin Game
 */
class GameResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'ttw' => GameService::SECONDS_TO_START,
            'creator' => new UserResource($this->whenLoaded('creator')),
            'opponent' => new UserResource($this->whenLoaded('opponent')),
            'winner_id' => $this->winner_id,
            'questions_count' => $this->game_questions_count ?: new MissingValue(),
            'points' => $this->getAttribute('points') ?? new MissingValue()
        ];
    }
}
