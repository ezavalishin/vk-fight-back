<?php


namespace App\Http\Resources;

use App\GameLog;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin GameLog
 */
class MyGameResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'creator' => [
                'points' => $this->user_one_points,
                'user' => new UserResource($this->whenLoaded('userOne'))
            ],
            'opponent' => [
                'points' => $this->user_two_points,
                'user' => new UserResource($this->whenLoaded('userTwo'))
            ],

            'created_at' => (string)$this->created_at
        ];
    }
}
