<?php


namespace App\Http\Resources;

use App\Game;
use App\GameUser;
use App\Services\GameService;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin Game
 */
class CloseGameResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'ttw' => GameService::SECONDS_TO_START,
            'creator' => new UserResource($this->whenLoaded('creator')),
            'opponent' => new UserResource($this->whenLoaded('opponent')),
            'winner_id' => $this->winner_id,
            'need_share' => $this->need_share ?? false
        ];
    }
}
