<?php


namespace App\Http\Resources;

use App\ErrorLog;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin ErrorLog
 */
class ErrorLogResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource($this->whenLoaded('user')),
            'payload' => $this->payload,
            'user_agent' => $this->user_agent,
            'created_at' => (string) $this->created_at
        ];
    }
}
