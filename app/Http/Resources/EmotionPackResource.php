<?php


namespace App\Http\Resources;

use App\Emotion;
use App\EmotionPack;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin EmotionPack
 */
class EmotionPackResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'emotions' => EmotionResource::collection($this->whenLoaded('emotions'))
        ];
    }
}
