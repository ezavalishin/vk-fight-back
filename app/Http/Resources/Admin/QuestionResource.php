<?php


namespace App\Http\Resources\Admin;

use App\Question;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;

/**
 * Class QuestionResource
 * @package App\Http\Resources\Admin
 * @mixin Question
 */
class QuestionResource extends JsonResource
{
    public function toArray($request)
    {
        $tags = $this->tags()->orderBy('title', 'asc')->get();

        return [
            'id' => $this->id,
            'title' => $this->title,
            'weight' => $this->weight,
            'games_count' => $this->game_questions_count ?? new MissingValue(),
            'right_answered_ratio' => $this->right_answered_ratio,
            'tags' => TagResource::collection($tags) ?? new MissingValue(),
            'question_db_id' => $this->question_db_id ?? new MissingValue()
        ];
    }
}
