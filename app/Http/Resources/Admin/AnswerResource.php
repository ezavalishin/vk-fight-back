<?php


namespace App\Http\Resources\Admin;

use App\Answer;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;

/**
 * Class AnswerResource
 * @package App\Http\Resources\Admin
 * @mixin Answer
 */
class AnswerResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'is_right' => $this->is_right,
            'games_count' => $this->game_answers_count ?? new MissingValue()
        ];
    }
}
