<?php


namespace App\Http\Resources\Admin;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\MissingValue;

/**
 * Class UserResource
 * @package App\Http\Resources
 * @mixin User
 */
class UserResource extends JsonResource
{
    /** @noinspection ProperNullCoalescingOperatorUsageInspection */
    public function toArray($request)
    {
        $rightRatio = new MissingValue();

        if (isset($this->right_game_answers_count, $this->game_answers_count)) {
            if ($this->game_answers_count === 0) {
                $rightRatio = 0;
            } else {
                $rightRatio = $this->right_game_answers_count / $this->game_answers_count;
            }
        }

        return [
            'id' => $this->id,
            'vk_user_id' => $this->vk_user_id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'avatar' => $this->avatar_200,
            'rating' => $this->rating,
            'game_users_count' => $this->game_users_count ?? new MissingValue(),
            'right_ratio' => $rightRatio
        ];
    }
}
