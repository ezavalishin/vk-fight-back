<?php


namespace App\Http\Controllers\Api;


use App\ErrorLog;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\UserResource;
use App\Http\Resources\ErrorLogResource;
use App\User;

class ErrorLogController extends Controller
{
    public function index()
    {
        $errorLogs = ErrorLog::query()->with('user')->orderByDesc('created_at');

        return ErrorLogResource::collection($errorLogs->paginate(150));
    }
}
