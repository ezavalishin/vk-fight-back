<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\UserResource;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::query()
            ->withCount(['gameUsers', 'gameAnswers', 'rightGameAnswers'])
            ->orderBy('id');

        return UserResource::collection($users->paginate(150));
    }
}
