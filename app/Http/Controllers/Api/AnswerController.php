<?php


namespace App\Http\Controllers\Api;

use App\Answer;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\AnswerResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AnswerController extends Controller
{
    public function delete($answerId) {
        Answer::whereId($answerId)->delete();

        return response()->json([], 204);
    }

    public function update(Request $request, $answerId) {
        $validator = Validator::make($request->all(), [
            'title' => 'string|max:255',
            'is_right' => 'boolean'
        ]);

        $answer = Answer::whereId($answerId)->first();
        $answer->update($validator->validated());
        $answer->save();

        return new AnswerResource($answer);
    }
}
