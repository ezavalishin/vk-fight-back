<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\TagResource;
use App\Tag;

class TagController extends Controller
{
    public function index() {
        $tags = Tag::orderBy('title', 'asc')->get();

        return TagResource::collection($tags);
    }
}
