<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\AnswerResource;
use App\Http\Resources\Admin\QuestionResource;
use App\Http\Resources\Admin\TagResource;
use App\Question;
use App\Tag;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\DomCrawler\Crawler;

class QuestionController extends Controller
{

    private const BAZA_OTVETOV = 'baza-otvetov';

    public function index()
    {
        $questions = Question::orderBy('id', 'asc')
            ->withCount('gameQuestions')
            ->paginate(20);


        return QuestionResource::collection($questions);
    }

    public function filter(Request $request)
    {
        $tagsIds = new Collection($request['tags']);

        $questions = Question::query()->whereHas('tags', function (Builder $query) use ($tagsIds) {
            $query->whereIn('id', $tagsIds);
        }, '>=', $tagsIds->count())
            ->orderBy('id', 'asc')
            ->withCount('gameQuestions')
            ->paginate(20);

        return QuestionResource::collection($questions);
    }

    public function search(Request $request)
    {
        $query = $request->query('q');
        $question = Question::orderBy('id', 'asc')
            ->withCount('gameQuestions')
            ->where('title', 'like', '%' . $query . '%')
            ->get();

        return QuestionResource::collection($question);
    }

    public function getAnswers($questionId)
    {
        $question = Question::whereId($questionId)->first();
        $answers = $question->answers()->withCount('gameAnswers')->orderBy('created_at', 'asc')->get();
        return AnswerResource::collection($answers);
    }

    public function delete($questionId)
    {
        Question::whereId($questionId)->delete();

        return response()->json([], 204);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|unique:questions|max:255',
            'weight' => 'integer',
            'tags' => 'array',
            'tags.title' => 'string',
            'answers' => 'array',
            'answers.title' => 'required|string|max:255',
            'answers.is_right' => 'boolean'
        ]);

        $tags = (new Collection($request['tags']))->map(function ($tag) {
            return Tag::query()->firstOrCreate([
                'title' => mb_strtolower($tag['title'])
            ]);
        });

        $question = Question::create([
            'title' => $request->title,
            'weight' => $request->weight
        ]);

        $question->tags()->sync($tags->pluck('id'));

        foreach ($request['answers'] as $answer) {
            $question->answers()->create([
                'title' => $answer['title'],
                'is_right' => $answer['is_right']
            ]);
        }

        if ($request->input('db')) {
            Storage::put('question-databases/' . $request->input('db') . '.txt', $request->input('question_db_id'));
        }

        $question->setAttribute('question_db_id', $request->input('question_db_id'));

        return new QuestionResource($question);
    }

    public function skipDatabase(Request $request) {
        Storage::put('question-databases/' . $request->input('db') . '.txt', $request->input('question_db_id'));

        return new JsonResource([
            'question_db_id' => $request->input('question_db_id')
        ]);
    }

    public function update(Request $request, $questionId)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'weight' => 'integer'
        ]);

        $question = Question::whereId($questionId)->first();
        $question->update($validator->validated());

        return new QuestionResource($question);
    }

    public function addAnswer(Request $request, $questionId)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'is_right' => 'boolean'
        ]);

        $question = Question::whereId($questionId)->first();
        $answer = $question->answers()->create($validator->validated());

        return new AnswerResource($answer);
    }

    public function getTags($questionId)
    {
        $question = Question::whereId($questionId)->first();
        $tags = $question->tags()->orderBy('title', 'asc')->get();

        return TagResource::collection($tags);
    }

    public function attachTag(Request $request, $questionId)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'string|max:255'
        ]);

        $question = Question::whereId($questionId)->first();
        $tag = Tag::query()->firstOrCreate([
            'title' => $request->title
        ]);
        $question->tags()->attach($tag->id);

        return new QuestionResource($question);
    }

    public function detachTag(Request $request, $questionId)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'string|max:255'
        ]);

        $question = Question::whereId($questionId)->first();
        $tag = Tag::whereTitle($request->title)->first();
        $question->tags()->detach($tag->id);

        return new QuestionResource($question);
    }

    public function getDatabaseLastId(Request $request)
    {
        try {
            $lastId = Storage::get('question-databases/' . $request->input('db') . '.txt');
        } catch (FileNotFoundException $e) {
            $lastId = 0;
        }

        return new JsonResource([
            'last_id' => $lastId
        ]);
    }


    public function getFromParser(Request $request)
    {
        $db = $request->input('db');
        $lastId = $request->input('last_id');

        if ($db === self::BAZA_OTVETOV) {
            $response = Http::get('https://baza-otvetov.ru/categories/view/1/' . $lastId);

            $crawler = new Crawler($response->body());

            $tr = $crawler->filter('.q-list__table tr:nth-of-type(2)');
            $id = trim($tr->filter('td:nth-of-type(1)')->text());
            $rightAnswer = $tr->filter('td:nth-of-type(3)')->text();
            $question = trim($tr->filter('td:nth-of-type(2) a')->text());

            $answers = new Collection(explode(',', trim(Str::after($tr->filter('td:nth-of-type(2) div')->text(), ':'))));

            $answers = $answers->map(function ($answer) {
                return [
                    'title' => trim($answer),
                    'is_right' => false
                ];
            })->take(2);

            $answers->push([
                'title' => trim($rightAnswer),
                'is_right' => true
            ]);

            $answers = $answers->shuffle();

            $existQuestion = Question::query()->where('title', 'ilike', $question)->first();

            return new JsonResource([
                'id' => $id,
                'question' => $question,
                'answers' => $answers->toArray(),
                'exists' => $existQuestion ? (new QuestionResource($existQuestion))->resolve() : false
            ]);
        }

    }
}
