<?php

namespace App\Http\Controllers\Api;

use App\Game;
use App\GameAnswer;
use App\Http\Controllers\Controller;
use App\Question;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Resources\Json\JsonResource;

class StatsController extends Controller
{
    public function index()
    {

        $usersCount = User::query()->count();
        $usersHasGameCount = User::query()->whereHas('gameUsers')->count();
        $notificationsAreEnabledCount = User::query()->where('notifications_are_enabled', true)->count();

        $gamesCount = Game::query()->count();
        $onlineGamesCount = Game::query()->where('is_online', true)->count();
        $activeGamesCount = Game::query()->where('is_active', true)->count();
        $avgGamesCountOnUsers = $usersHasGameCount > 0 ? $gamesCount / $usersHasGameCount : 0;


        $gameAnswersCount = GameAnswer::query()->count();
        $questionsCount = Question::query()->count();
        $rightAnswersRatio = $gameAnswersCount > 0 ? (GameAnswer::query()
                ->whereHas('answer', function (Builder $q) {
                    return $q->where('is_right', true);
                })->count() / $gameAnswersCount) : 0;
        $hardQuestionsCount = Question::query()->where('weight', Question::HARD)->count();
        $mediumQuestionsCount = Question::query()->where('weight', Question::MEDIUM)->count();
        $easyQuestionsCount = Question::query()->where('weight', Question::EASY)->count();


        $stats = [
            'Пользователи' => [
                'Кол-во' => $usersCount,
                'Сыгравших игру' => $usersHasGameCount,
                'Включены уведомления' => $notificationsAreEnabledCount
            ],
            'Игры' => [
                'Кол-во' => $gamesCount,
                'Активных' => $activeGamesCount,
                'Всего онлайн игр' => $onlineGamesCount,
                'Среднее кол-во на игрока' => round($avgGamesCountOnUsers, 2)
            ],
            'Вопросы' => [
                'Кол-во' => $questionsCount,
                'Легких' => $easyQuestionsCount,
                'Средних' => $mediumQuestionsCount,
                'Сложных' => $hardQuestionsCount,
                'Процент верных ответов' => round($rightAnswersRatio * 100, 2),

            ]
        ];

        return new JsonResource($stats);
    }
}
