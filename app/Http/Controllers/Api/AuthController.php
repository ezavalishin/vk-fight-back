<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\VkClient;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use VK\Client\VKApiClient;

class AuthController extends Controller
{
    public function auth(Request $request)
    {
        $client = new VKApiClient(VkClient::API_VERSION, VkClient::LANG);

        $user = $client->users()->get($request->accessToken);

        if (!isset($user[0]) && !isset($user[0]['id'])) {
            abort(400);
        }

        $user = User::byVkId($user[0]['id']);


        if (!$user->is_admin) {
            abort(403);
        }

        $payload = [
            'iss' => 'lumen-jwt',
            'sub' => $user->id,
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60 * 60 * 7 // Expiration time
        ];

        $response = new Collection([
            'access_token' => JWT::encode($payload, env('JWT_SECRET'))
        ]);


        return new JsonResource($response);
    }
}
