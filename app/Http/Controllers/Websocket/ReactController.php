<?php

namespace App\Http\Controllers\Websocket;

use App\Emotion;
use App\Game;
use App\Http\Controllers\Controller;
use App\User;
use SwooleTW\Http\Websocket\Websocket;

class ReactController extends Controller
{
    public function sendReact(Websocket $websocket, $data)
    {
        $user = User::query()->find(\SwooleTW\Http\Websocket\Facades\Websocket::getUserId());

        $gameId = $data['game_id'];
        $emotionId = $data['emotion_id'];

        $game = Game::query()->find($gameId);

        if (!$game) {
            return;
        }

        $emotion = Emotion::query()->find($emotionId);

        if (!$emotion) {
            return;
        }

        if (!$user->emotionPacks()->wherePivot('emotion_pack_id', $emotion->emotion_pack_id)->exists()) {
            info('has no');
            return;
        }

        $websocket->to('game_' . $game->id)->emit('got-react', [
            'emotion_url' => $emotion->getLink(),
            'from_id' => $user->id
        ]);
    }
}
