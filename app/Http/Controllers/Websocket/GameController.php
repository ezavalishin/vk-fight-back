<?php

namespace App\Http\Controllers\Websocket;

use App\Answer;
use App\Exceptions\WebsocketErrorException;
use App\Game;
use App\GameQuestion;
use App\GameUser;
use App\Http\Controllers\Controller;
use App\Http\Resources\GameResource;
use App\Http\Resources\QuestionResource;
use App\Http\Resources\UserResource;
use App\Services\GameService;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use SwooleTW\Http\Websocket\Websocket;

class GameController extends Controller
{
    private static function gameInviteKey(int $gameId, int $recipientId): string
    {
        return 'invite_' . $gameId . '_' . $recipientId;
    }

    private function errorMessage(User $user, string $message, int $code): void
    {
        \SwooleTW\Http\Websocket\Facades\Websocket::toUser($user)->emit(WebsocketErrorException::ERROR_EVENT, [
            'message' => $message,
            'code' => $code
        ]);
    }

    private function errorException(User $user, WebsocketErrorException $errorException): void
    {
        \SwooleTW\Http\Websocket\Facades\Websocket::toUser($user)->emit(WebsocketErrorException::ERROR_EVENT, [
            'message' => $errorException->getMessage(),
            'code' => $errorException->getCode()
        ]);
    }

    public function createOnlineGame(Websocket $websocket, $data)
    {
        $user = User::query()->find(\SwooleTW\Http\Websocket\Facades\Websocket::getUserId());

        $game = GameService::createOnline($user);

        $websocket->join('game_' . $game->id);

        $websocket->to('game_' . $game->id)->emit('game-created', (new GameResource($game))->resolve());
    }

    public function startOfflineGame(Websocket $websocket, $data)
    {
        $user = User::query()->find(\SwooleTW\Http\Websocket\Facades\Websocket::getUserId());

        $game = GameService::createOffline($user);

        $websocket->join($user->roomChannel($game->id));

        $websocket->to($user->roomChannel($game->id))->emit('game-created', (new GameResource($game))->resolve());

        GameService::startGame($game, $user);
    }

    public function connetToOnlineGame(Websocket $websocket, $data)
    {
        $user = User::query()->find(\SwooleTW\Http\Websocket\Facades\Websocket::getUserId());

        $gameId = (int)($data['game_id'] ?? null);
        $gameUuid = $data['game_uuid'] ?? null;

        $game = null;
        if ($gameId) {
            $game = Game::query()->find($gameId);
        }
        if ($gameUuid) {
            $game = Game::query()->whereNotNull('uuid')->where('uuid', $gameUuid)->first();
        }

        if (!$game) {
            $this->errorException($user, WebsocketErrorException::gameNotFound());
            return;
        }

        try {
            if ($game->is_online) {
                $game = GameService::connectToOnline($game, $user);
            } else {
                $game = GameService::connectToOfflineGame($game, $user);
            }

            if ($game->is_online) {
                $websocket->join('game_' . $game->id);
                $websocket->to('game_' . $game->id)->emit('player-connected', (new UserResource($user))->resolve());
            } else {
                $websocket->join($user->roomChannel($game->id));
                $websocket->to($user->roomChannel($game->id))->emit('player-connected', (new UserResource($user))->resolve());
            }

            if ($game->is_online) {
                if ($game->is_active) {
                    GameService::restartActiveGame($game);
                } else {
                    GameService::startGame($game);
                }
            } else {

                if ($game->is_active) {
                    GameService::restartActiveGame($game, $user);
                } else {
                    GameService::startGame($game, $user);
                }
            }
        } catch (WebsocketErrorException $e) {
            $this->errorException($user, $e);
        }
    }

    private function fastPhrase()
    {
        return (new Collection([
            'Быстрые ручки!',
            'Быстряк!',
            'Вот это реакция!'
        ]))->random();
    }

    public function answer(Websocket $websocket, $data)
    {
        $user = User::query()->find(\SwooleTW\Http\Websocket\Facades\Websocket::getUserId());

        $gameId = $data['game_id'];
        $answerId = $data['answer_id'];

        $game = Game::query()->find($gameId);
        $answer = Answer::query()->find($answerId);

        $gameAnswer = GameService::setAnswer($game, $answer, $user);

        if (!$gameAnswer) {
            return;
        }

        if ($gameAnswer->is_first) {
            $websocket->toUser($user)->emit('badge', [
                'message' => $this->fastPhrase()
            ]);
        }
    }

    public function sendInvite(Websocket $websocket, $data)
    {
        $user = User::query()->find(\SwooleTW\Http\Websocket\Facades\Websocket::getUserId());

        $gameId = $data['game_id'];
        $opponentId = $data['user_id'];

        $game = Game::query()->find($gameId);

        if (!$game) {
            $this->errorException($user, WebsocketErrorException::gameNotFound());
            return;
        }

        $opponent = User::query()->find($opponentId);

        if (!$opponent) {
            $this->errorException($user, WebsocketErrorException::userNotFound());
            return;
        }

        if (Cache::has(self::gameInviteKey($game->id, $opponent->id)) && Cache::get(self::gameInviteKey($game->id, $opponent->id) > 1)) {
            return;
        }

        if (!Cache::has(self::gameInviteKey($game->id, $opponent->id))) {
            Cache::put(self::gameInviteKey($game->id, $opponent->id), 0, Carbon::now()->addMinutes(15));
        }

        if (\SwooleTW\Http\Websocket\Facades\Websocket::isUserIdOnline($opponent->id)) {
            $websocket->toUser($opponent)->emit('game-invite', [
                'game_id' => $game->id,
                'creator' => (new UserResource($user))->resolve()
            ]);
        } else {
            $this->errorException($user, WebsocketErrorException::userIsOffline());
        }
    }

    public function sendInviteByLastGameId(Websocket $websocket, $data)
    {
        $user = User::query()->find(\SwooleTW\Http\Websocket\Facades\Websocket::getUserId());

        $gameId = $data['game_id'];
        $lastGameId = $data['last_game_id'];

        $game = Game::query()->find($gameId);

        if (!$game) {
            $this->errorException($user, WebsocketErrorException::gameNotFound());
            return;
        }

        $lastGame = Game::query()->find($lastGameId);

        if (!$lastGame) {
            $this->errorException($user, WebsocketErrorException::gameNotFound());
            return;
        }

        $lastGameGameUsers = $lastGame->gameUsers()->with('user')->get();

        $lastGameGameUsers->each(function (GameUser $gameUser) use ($user, $game, $websocket) {

            $opponent = $gameUser->user;

            if (\SwooleTW\Http\Websocket\Facades\Websocket::isUserIdOnline($opponent->id)) {
                $websocket->toUser($gameUser->user)->emit('game-invite', [
                    'game_id' => $game->id,
                    'creator' => (new UserResource($user))->resolve()
                ]);
            }
        });
    }

    public function declineInvite(Websocket $websocket, $data)
    {
        $user = User::query()->find(\SwooleTW\Http\Websocket\Facades\Websocket::getUserId());

        $gameId = $data['game_id'];

        $game = Game::query()->find($gameId);

        if (!$game) {
            $this->errorException($user, WebsocketErrorException::gameNotFound());
            return;
        }

        $websocket->to('game_' . $game->id)->emit('invite-declined', (new GameResource($game))->resolve());

        Cache::increment(self::gameInviteKey($game->id, $user->id));
    }


    public function getActiveGame(Websocket $websocket)
    {
        $user = User::query()->find(\SwooleTW\Http\Websocket\Facades\Websocket::getUserId());

        /**
         * @var GameUser $activeUserGame
         */
        $activeUserGame = $user->gameUsers()
            ->whereNotNull('started_at')
            ->whereNull('finished_at')
            ->first();

        $isOnline = $activeUserGame ? $activeUserGame->game->is_online : null;

        $activeGameId = $activeUserGame ? $activeUserGame->game_id : null;

        $websocket->toUser($user)->emit('active-game', (new JsonResource([
            'game_id' => $activeGameId,
            'is_online' => $isOnline
        ]))->resolve());
    }

    public function getActiveQuestion(Websocket $websocket)
    {
        $user = User::query()->find(\SwooleTW\Http\Websocket\Facades\Websocket::getUserId());

        /**
         * @var GameUser $activeUserGame
         */
        $activeUserGame = $user->gameUsers()
            ->whereNotNull('started_at')
            ->whereNull('finished_at')
            ->first();

        if (!$activeUserGame) {
            $this->errorException($user, WebsocketErrorException::gameNotFound());
        }

        $game = $activeUserGame->game;


        /**
         * @var GameQuestion $activeGameQuestion
         */
        $activeGameQuestion = $game->gameQuestions()->where('is_active', true)->first();


        if (!$activeGameQuestion) {
            $websocket->toUser($user)->emit('active-question', [
                'id' => null
            ]);
            return;
        }

        $order = $game->gameQuestions()->where('id', '<=', $activeGameQuestion->id)->count();

        $activeQuestion = $activeGameQuestion->question;

        $activeQuestion->setAttribute('order', $order);

        $websocket->toUser($user)
            ->emit('active-question', (new QuestionResource($activeQuestion))->resolve());
    }
}
