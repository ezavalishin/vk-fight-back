<?php

namespace App\Http\Controllers\VkUser;

use App\Game;
use App\GameLog;
use App\GameUser;
use App\Http\Controllers\Controller;
use App\Http\Resources\MyGameResource;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{
    public function myGames()
    {
        $user = Auth::user();

        $games = GameLog::query()
            ->with(['userOne', 'userTwo'])
            ->orderByDesc('created_at')
            ->where(function (Builder $query) use ($user) {
                $query->where('user_one_id', $user->id)
                    ->orWhere('user_two_id', $user->id);
            })
            ->paginate();

        return MyGameResource::collection($games);
    }
}
