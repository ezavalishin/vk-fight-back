<?php

namespace App\Http\Controllers\VkUser;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function rating(Request $request)
    {
        $builder = User::query()->orderByDesc('rating');

        if (!$request->has('friends')) {
            return UserResource::collection($builder->take(10)->get());
        }

        $user = Auth::user();

        if (!$user) {
            return;
        }

        $friendIds = $user->friends()->select('id')->get()->pluck('id');

        $friendIds->push($user->id);

        $builder = $builder->whereIn('id', $friendIds);

        return UserResource::collection($builder->take(10)->get());
    }

    public function getFriends()
    {
        $user = Auth::user();

        if (!$user) {
            return;
        }

        $friends = $user->friends()->get();

        return UserResource::collection($friends);
    }
}
