<?php

namespace App\Http\Controllers\VkUser;

use App\ErrorLog;
use App\GameLog;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ErrorLogController extends Controller
{
    public function store(Request $request)
    {
        $user = Auth::user();

        if (!$user) {
            return;
        }

        if ($request->has('payload')) {
            ErrorLog::query()->create([
                'user_id' => $user->id,
                'payload' => $request->input('payload'),
                'user_agent' => $request->userAgent()
            ]);
        }
    }
}
