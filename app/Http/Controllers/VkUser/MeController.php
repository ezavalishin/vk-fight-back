<?php

namespace App\Http\Controllers\VkUser;

use App\GameLog;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class MeController extends Controller
{
    public function auth()
    {
        $user = Auth::user();

        if (!$user) {
            return;
        }

        $gamesCount = GameLog::query()->where('user_one_id', $user->id)
            ->orWhere('user_two_id', $user->id)
            ->count();



        $winsCount = GameLog::query()->where(function (Builder $q) use ($user) {
            $q->where('user_one_id', $user->id)
                ->whereRaw('user_one_points > user_two_points');
        })
            ->orWhere(function (Builder $q) use ($user) {
                $q->where('user_two_id', $user->id)
                    ->whereRaw('user_two_points > user_one_points');
            })
            ->count();


        $friendIds = $user->friends()->select('id')->get()->pluck('id');

        $globalPlace = User::query()->where('rating', '>', $user->rating)->count() + 1;
        $friendPlace = User::query()->whereIn('id', $friendIds)->where('rating', '>', $user->rating)->count() + 1;


        $user->load(['friends', 'emotionPacks.emotions']);

        $user->setAttribute('gamesCount', $gamesCount);
        $user->setAttribute('winsCount', $winsCount);
        $user->setAttribute('globalPlace', $globalPlace);
        $user->setAttribute('friendPlace', $friendPlace);

        return new UserResource(Auth::user());
    }
}
