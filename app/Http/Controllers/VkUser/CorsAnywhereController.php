<?php

namespace App\Http\Controllers\VkUser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CorsAnywhereController extends Controller
{
    public function post(Request $request)
    {
        $url = $request->query('url');

        if (Str::contains(mb_strtolower($url), ['localhost', '127.0.0.1'])) {
            abort(403);
        }

        $http = Http::asMultipart();

        /**
         * @var UploadedFile $file
         */


        $fileNames = [];

        foreach ($request->allFiles() as $key => $file) {
            $fileName = $file->store('temp');
            $http = $http->attach($key, Storage::readStream($fileName));

            $fileNames[] = $fileName;
        }

        $response = $http->post($url);


        foreach ($fileNames as $fn) {
            Storage::delete($fn);
        }

        return new Response($response->body(), $response->status(), $response->headers());
    }
}
