<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;


/**
 * App\Answer
 *
 * @property int $id
 * @property int $question_id
 * @property string $title
 * @property bool $is_right
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Answer newModelQuery()
 * @method static Builder|Answer newQuery()
 * @method static Builder|Answer query()
 * @method static Builder|Answer whereCreatedAt($value)
 * @method static Builder|Answer whereId($value)
 * @method static Builder|Answer whereIsRight($value)
 * @method static Builder|Answer whereQuestionId($value)
 * @method static Builder|Answer whereTitle($value)
 * @method static Builder|Answer whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Question $question
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\GameAnswer[] $gameAnswers
 * @property-read int|null $game_answers_count
 */
class Answer extends Model
{
    protected $fillable = [
        'title',
        'is_right'
    ];

    public function question(): BelongsTo
    {
        return $this->belongsTo(Question::class);
    }

    public function gameAnswers(): HasMany
    {
        return $this->hasMany(GameAnswer::class);
    }
}
