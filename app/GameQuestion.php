<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\GameQuestion
 *
 * @property int $id
 * @property int $game_id
 * @property int $question_id
 * @property bool $is_active
 * @property string|null $started_at
 * @property string|null $finished_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|GameQuestion newModelQuery()
 * @method static Builder|GameQuestion newQuery()
 * @method static Builder|GameQuestion query()
 * @method static Builder|GameQuestion whereCreatedAt($value)
 * @method static Builder|GameQuestion whereFinishedAt($value)
 * @method static Builder|GameQuestion whereGameId($value)
 * @method static Builder|GameQuestion whereId($value)
 * @method static Builder|GameQuestion whereIsActive($value)
 * @method static Builder|GameQuestion whereQuestionId($value)
 * @method static Builder|GameQuestion whereStartedAt($value)
 * @method static Builder|GameQuestion whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Question $question
 * @property-read Collection|GameAnswer[] $gameAnswers
 * @property-read int|null $game_answers_count
 * @property int $creator_points
 * @property int $opponent_points
 * @method static Builder|GameQuestion whereCreatorPoints($value)
 * @method static Builder|GameQuestion whereOpponentPoints($value)
 * @property-read Game $game
 */
class GameQuestion extends Model
{
    protected $fillable = [
        'question_id',
        'game_id',
        'is_active',
        'started_at',
        'finished_at'
    ];

    protected $casts = [
        'creator_points' => 'integer',
        'opponent_points' => 'integer',
    ];

    public function question(): BelongsTo
    {
        return $this->belongsTo(Question::class);
    }

    public function gameAnswers(): HasMany
    {
        return $this->hasMany(GameAnswer::class);
    }

    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class);
    }
}
