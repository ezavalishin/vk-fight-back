<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;


/**
 * App\GameLog
 *
 * @property int $id
 * @property int $game_id
 * @property int $user_one_id
 * @property int $user_two_id
 * @property int $user_one_points
 * @property int $user_two_points
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Game $game
 * @property-read User $userOne
 * @property-read User $userTwo
 * @method static Builder|GameLog newModelQuery()
 * @method static Builder|GameLog newQuery()
 * @method static Builder|GameLog query()
 * @method static Builder|GameLog whereCreatedAt($value)
 * @method static Builder|GameLog whereGameId($value)
 * @method static Builder|GameLog whereId($value)
 * @method static Builder|GameLog whereUpdatedAt($value)
 * @method static Builder|GameLog whereUserOneId($value)
 * @method static Builder|GameLog whereUserOnePoints($value)
 * @method static Builder|GameLog whereUserTwoId($value)
 * @method static Builder|GameLog whereUserTwoPoints($value)
 * @mixin Eloquent
 */
class GameLog extends Model
{
    protected $fillable = [
        'game_id',
        'user_one_id',
        'user_two_id',
        'user_one_points',
        'user_two_points',
    ];

    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class);
    }

    public function userOne(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_one_id');
    }

    public function userTwo(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_two_id');
    }
}
