<?php

namespace App\Exceptions;

class WebsocketErrorException extends \Exception
{
    public const ERROR_EVENT = 'exception';

    public const GAME_ALREADY_STARTED = 1;
    public const GAME_ALREADY_FINISHED = 2;
    public const GAME_ALREADY_PLAYED = 3;
    public const USER_IS_OFFLINE = 4;
    public const GAME_NOT_FOUND = 5;
    public const USER_NOT_FOUND = 6;
    public const SELF_GAME = 7;
    public const CREATOR_HAS_ACTIVE_GAME = 8;

    public static function gameAlreadyStarted(): WebsocketErrorException
    {
        return new self('Игра уже началась', self::GAME_ALREADY_STARTED);
    }

    public static function gameAlreadyFinished(): WebsocketErrorException
    {
        return new self('Игра уже завершилась', self::GAME_ALREADY_FINISHED);
    }

    public static function gameAlreadyPlayed(): WebsocketErrorException
    {
        return new self('Игра уже была сыграна', self::GAME_ALREADY_PLAYED);
    }

    public static function userIsOffline(): WebsocketErrorException
    {
        return new self('Игрок не в сети', self::USER_IS_OFFLINE);
    }

    public static function gameNotFound(): WebsocketErrorException
    {
        return new self('Игра не найдена', self::GAME_NOT_FOUND);
    }

    public static function userNotFound(): WebsocketErrorException
    {
        return new self('Игрок не найдена', self::USER_NOT_FOUND);
    }

    public static function selfGame(): WebsocketErrorException
    {
        return new self('Игра с самим собой', self::SELF_GAME);
    }

    public static function creatorHasActiveGame() : WebsocketErrorException {
        return new self('Игрок уже начал игру', self::CREATOR_HAS_ACTIVE_GAME);
    }

}
