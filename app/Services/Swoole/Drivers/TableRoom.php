<?php

namespace App\Services\Swoole\Drivers;

class TableRoom extends \SwooleTW\Http\Websocket\Rooms\TableRoom {
    public function getRoomsCount() {
        return $this->rooms->count();
    }

    public function getFdsCount() {
        return $this->fds->count();
    }

    public function closeAndClearRoom($room) {
        $this->rooms->del($room);
    }

}
