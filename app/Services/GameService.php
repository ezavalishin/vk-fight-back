<?php

namespace App\Services;

use App\Answer;
use App\Exceptions\WebsocketErrorException;
use App\Game;
use App\GameAnswer;
use App\GameQuestion;
use App\GameUser;
use App\Jobs\CloseQuestionInGame;
use App\Jobs\SendGameClosed;
use App\Jobs\SendGamePreview;
use App\Jobs\SendGameReady;
use App\Jobs\SendGameStarted;
use App\Jobs\SendQuestionToGame;
use App\Question;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;


class GameService
{
    public const SECONDS_TO_START = 3;
    public const QUESTION_TO_ANSWER = 10;
    public const ANSWER_TO_SHOW = 3;

    private static function questionByWeightCount(): array
    {
        return [
            Question::EASY => 4,
            Question::MEDIUM => 2,
            Question::HARD => 1
        ];
    }

    public static function createOnline(User $user): Game
    {
        return self::createGame($user, true);
    }

    public static function createOffline(User $user): Game
    {
        return self::createGame($user, false);
    }

    public static function gameSeconds() {
        $result = self::SECONDS_TO_START;

        $questionsCount = (new Collection(self::questionByWeightCount()))->reduce(function($carry, $count) {
            return $carry + $count;
        }, 0);

        $result += $questionsCount * (self::QUESTION_TO_ANSWER + self::ANSWER_TO_SHOW);

        return $result;
    }

    private static function createGame(User $user, bool $isOnline): Game
    {
        if (!$isOnline) {
            $existsGame = Game::query()
                ->where('is_online', false)
                ->where('creator_id', '!=', $user->id)
                ->whereDoesntHave('gameUsers', function (Builder $q) {
                    $q->whereRaw('games.creator_id != game_users.user_id');
                })
                ->first();

            if ($existsGame) {

                self::attachUserToGame($user, $existsGame);

                return $existsGame;
            }
        }


        $game = Game::query()->create([
            'creator_id' => $user->id,
            'is_online' => $isOnline
        ]);

        $preparedQuestions = self::generateQuestionPool($user);

        $preparedQuestions->each(function (Question $question) use ($game) {
            $game->gameQuestions()->create([
                'question_id' => $question->id
            ]);
        });

        self::attachUserToGame($user, $game);

        return $game;
    }

    private static function generateQuestionPool(User $user): Collection
    {
        $questions = new Collection();
        $gotTags = new Collection();

        $userGameIds = $user->gameUsers()->select(['game_id'])->get()->pluck('game_id');
        $userQuestionIds = GameQuestion::query()
            ->whereIn('game_id', $userGameIds)
            ->distinct('question_id')
            ->select(['question_id'])
            ->get()
            ->pluck('question_id');

        foreach (self::questionByWeightCount() as $weight => $count) {

            for ($i = 0; $i < $count; $i++) {
                $result = Question::whereWeight($weight)
                    ->whereNotIn('id', $userQuestionIds->merge($questions->pluck('id')))
                    ->inRandomOrder()
                    ->whereDoesntHave('tags', function (Builder $q) use ($gotTags) {
                        $q->whereIn('id', $gotTags);
                    })
                    ->first();

                if (!$result) {
                    $result = Question::whereWeight($weight)
                        ->whereNotIn('id', $userQuestionIds->merge($questions->pluck('id')))
                        ->inRandomOrder()
                        ->first();
                }

                if (!$result) {
                    $result = Question::whereWeight($weight)
                        ->whereNotIn('id', $questions->pluck('id'))
                        ->inRandomOrder()
                        ->first();
                }

                $gotTags->push(...$result->tags->pluck('id')->toArray());
                $questions->push($result);
            }
        }

        return $questions;
    }

    /**
     * @param Game $game
     * @param User $user
     * @return Game
     * @throws WebsocketErrorException
     */
    public static function connectToOfflineGame(Game $game, User $user): Game
    {
        self::veryOfflineGameToConnect($game, $user);

        self::attachUserToGame($user, $game);

        return $game;
    }


    /**
     * @param Game $game
     * @param User $user
     * @return Game
     * @throws WebsocketErrorException
     */
    public static function connectToOnline(Game $game, User $user): Game
    {
        self::veryOnlineGameToConnect($game, $user);

        self::attachUserToGame($user, $game);

        return $game;
    }

    /**
     * @param Game $game
     * @param User $user
     * @throws WebsocketErrorException
     */
    private static function veryOnlineGameToConnect(Game $game, User $user): void
    {

        if (!$game->is_active && config('app.env') === 'production' && $game->gameUsers()->where('user_id', $user->id)->exists()) {
            throw WebsocketErrorException::selfGame();
        }

        if ($game->is_active && !$game->gameUsers()->where('user_id', $user->id)->exists()) {
            throw WebsocketErrorException::gameAlreadyStarted();
        }

        if (!$game->is_active && $game->finished_at !== null) {
            throw WebsocketErrorException::gameAlreadyFinished();
        }

        if ($game->creator->hasActiveGame()) {
            throw WebsocketErrorException::creatorHasActiveGame();
        }
    }

    /**
     * @param Game $game
     * @param User $user
     * @throws WebsocketErrorException
     */
    private static function veryOfflineGameToConnect(Game $game, User $user): void
    {
        $alreadyPlayed = $game->gameUsers()
            ->where('user_id', $user->id)
            ->whereNotNull('finished_at')
            ->exists();

        if ($alreadyPlayed) {
            throw WebsocketErrorException::gameAlreadyPlayed();
        }
    }


    public static function startGame(Game $game, User $offlineUser = null)
    {
        $now = Carbon::now();

        dispatch(new SendGameReady($game, $offlineUser));

        dispatch(new SendGamePreview($game, $offlineUser));

        dispatch((new SendGameStarted($game))->delay($now->addSeconds(self::SECONDS_TO_START)));

        $gameQuestions = $game->gameQuestions;

        $now->addSecond();

        $i = 1;
        $gameQuestions->each(function (GameQuestion $gameQuestion) use ($now, $offlineUser, &$i) {
            dispatch((new SendQuestionToGame($gameQuestion, $offlineUser, $i))->delay($now));
            dispatch((new CloseQuestionInGame($gameQuestion, $offlineUser))->delay($now->addSeconds(self::QUESTION_TO_ANSWER)));
            $now->addSeconds(self::ANSWER_TO_SHOW);
            $i++;
        });

        dispatch((new SendGameClosed($game, $offlineUser))->delay($now));
    }

    public static function restartActiveGame(Game $game, User $offlineUser = null)
    {
        dispatch(new SendGameReady($game, $offlineUser));
        dispatch(new SendGamePreview($game, $offlineUser));
    }


    public static function setAnswer(Game $game, Answer $answer, User $user): ?GameAnswer
    {
        /**
         * @var GameQuestion $gameQuestion
         */
        $gameQuestion = $game->gameQuestions()->where('question_id', $answer->question_id)->first();

        /**
         * @var GameUser $gameUser
         */
        $gameUser = $game->gameUsers()->where('user_id', $user->id)->first();

        if (!$gameUser) {
            echo 'no user';
            return null;
        }

        $exists = GameAnswer::query()->where('game_question_id', $gameQuestion->id)->where('user_id', $user->id)->exists();

        if ($exists) {
            return null;
        }

        $isFirst = $gameUser->last_question_at->diffInSeconds(Carbon::now(), true) <= 3;


        if ($answer->is_right) {
            $points = (int)$answer->question->weight;

            if ($isFirst) {
                ++$points;
            }

            $gameUser->points += $points;

            $gameUser->save();
        }

        return GameAnswer::query()->create([
            'game_question_id' => $gameQuestion->id,
            'user_id' => $user->id,
            'answer_id' => $answer->id,
            'answered_at' => Carbon::now(),
            'is_first' => $isFirst
        ]);
    }


    private static function attachUserToGame(User $user, Game $game): Model
    {
        return $game->gameUsers()->firstOrCreate([
            'user_id' => $user->id
        ]);
    }
}
