<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;


/**
 * App\ErrorLog
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $user_agent
 * @property string $payload
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|ErrorLog newModelQuery()
 * @method static Builder|ErrorLog newQuery()
 * @method static Builder|ErrorLog query()
 * @method static Builder|ErrorLog whereCreatedAt($value)
 * @method static Builder|ErrorLog whereId($value)
 * @method static Builder|ErrorLog wherePayload($value)
 * @method static Builder|ErrorLog whereUpdatedAt($value)
 * @method static Builder|ErrorLog whereUserAgent($value)
 * @method static Builder|ErrorLog whereUserId($value)
 * @mixin Eloquent
 */
class ErrorLog extends Model
{
    protected $fillable = [
        'user_agent',
        'payload',
        'user_id'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
