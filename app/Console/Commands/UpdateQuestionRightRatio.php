<?php

namespace App\Console\Commands;

use App\Question;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class UpdateQuestionRightRatio extends Command
{
    protected $signature = 'questions:update_ratio';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update question right ratio';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Question::query()->chunk(100, function(Collection $questions) {
           $questions->each(function(Question $question) {
                $question->updateRightAnswerRatio();
           });
        });
    }
}
