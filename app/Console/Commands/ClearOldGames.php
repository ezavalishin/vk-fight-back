<?php

namespace App\Console\Commands;

use App\Game;
use App\Services\GameService;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ClearOldGames extends Command
{
    protected $signature = 'games:clear_old';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear old not started games';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // not started games
        Game::query()
            ->whereNull('started_at')
            ->where('created_at', '<', Carbon::now()->subHour())
            ->delete();


        // not finished games
        Game::query()
            ->whereNotNull('started_at')
            ->where('is_active', true)
            ->where('started_at', '<', Carbon::now()->subSeconds(GameService::gameSeconds() + 10))
            ->each(function (Game $game) {
                $game->is_active = false;
                $game->finished_at = Carbon::now();
                $game->save();

                $game->gameUsers()->update([
                    'finished_at' => Carbon::now()
                ]);
            });
    }
}
