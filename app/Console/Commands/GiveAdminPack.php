<?php

namespace App\Console\Commands;

use App\EmotionPack;
use App\Game;
use App\Services\GameService;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GiveAdminPack extends Command
{
    protected $signature = 'emotions:admin {userId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Give admin pack';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $adminPack = EmotionPack::query()->where('slug', 'admin')->first();

        if (!$adminPack) {
            $this->error('pack not found');
            return;
        }

        $userId = $this->argument('userId');
        $user = User::query()->find($userId);

        if (!$user) {
            $this->error('user not found');
            return;
        }

        $user->emotionPacks()->attach($adminPack);
    }
}
