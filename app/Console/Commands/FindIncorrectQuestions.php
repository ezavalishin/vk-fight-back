<?php

namespace App\Console\Commands;

use App\Question;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class FindIncorrectQuestions extends Command
{
    protected $signature = 'questions:incorrect';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Find incorrect questions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $incorrectQuestions = Question::query()->whereHas('answers', null, '<', 3)->select('id');

        $this->info('answers count less 3: ' . $incorrectQuestions->pluck('id')->toJson());


        $incorrectQuestions = Question::query()->whereHas('answers', function (Builder $q) {
            $q->where('is_right', true);
        }, '!=', 1)->select('id');

        $this->info('incorrect right answer count: ' . $incorrectQuestions->pluck('id')->toJson());
    }
}
