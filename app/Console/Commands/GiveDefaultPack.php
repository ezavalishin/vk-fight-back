<?php

namespace App\Console\Commands;

use App\EmotionPack;
use App\Game;
use App\Services\GameService;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class GiveDefaultPack extends Command
{
    protected $signature = 'emotions:default';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Give default pack';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $defaultPack = EmotionPack::query()->where('slug', 'default')->first();

        if (!$defaultPack) {
            $this->error('pack not found');
            return;
        }

        $users = User::query()->whereDoesntHave('emotionPacks', function(Builder $q) use ($defaultPack) {
           $q->where('emotion_packs.id', $defaultPack->id);
        })->get();

        $users->each(function (User $user) use ($defaultPack) {
           $user->emotionPacks()->attach($defaultPack->id);
        });
    }
}
