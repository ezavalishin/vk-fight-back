<?php

namespace App\Console;

use App\Console\Commands\ClearOldGames;
use App\Console\Commands\FindIncorrectQuestions;
use App\Console\Commands\GiveAdminPack;
use App\Console\Commands\GiveDefaultPack;
use App\Console\Commands\StorageLinkCommand;
use App\Console\Commands\UpdateQuestionRightRatio;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        UpdateQuestionRightRatio::class,
        ClearOldGames::class,
        StorageLinkCommand::class,
        FindIncorrectQuestions::class,
        GiveAdminPack::class,
        GiveDefaultPack::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('questions:update_ratio')->daily();
        $schedule->command('games:clear_old')->everyMinute();
    }
}
