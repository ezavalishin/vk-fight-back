<?php

namespace App\Jobs;

use App\Game;
use App\Http\Resources\GameResource;
use App\User;
use Illuminate\Support\Carbon;
use SwooleTW\Http\Websocket\Facades\Websocket;

class SendGameReady extends Job
{

    protected $game;
    protected $offlineUser;

    /**
     * Create a new job instance.
     *
     * @param Game $game
     * @param User $offlineUser
     */
    public function __construct(Game $game, User $offlineUser = null)
    {
        $this->game = $game;
        $this->offlineUser = $offlineUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if ($this->offlineUser) {
            Websocket::to($this->offlineUser->roomChannel($this->game->id))->emit('game-ready', []);
        } else {
            Websocket::to('game_' . $this->game->id)->emit('game-ready', []);
        }
    }
}
