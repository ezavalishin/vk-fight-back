<?php

namespace App\Jobs;

use App\GameAnswer;
use App\GameQuestion;
use App\Http\Resources\GameAnswerResource;
use App\Services\GameService;
use App\User;
use Illuminate\Support\Carbon;
use SwooleTW\Http\Websocket\Facades\Websocket;

class CloseQuestionInGame extends Job
{
    protected $gameQuestion;
    protected $offlineUser;

    /**
     * Create a new job instance.
     *
     * @param GameQuestion $gameQuestion
     * @param User $offlineUser
     */
    public function __construct(GameQuestion $gameQuestion, User $offlineUser = null)
    {
        $this->gameQuestion = $gameQuestion;
        $this->offlineUser = $offlineUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->gameQuestion->is_active = false;
        $this->gameQuestion->finished_at = Carbon::now();

        $this->gameQuestion->save();

        if ($this->offlineUser) {
            $gameAnswers = $this->gameQuestion->gameAnswers()->whereIn('user_id', [$this->gameQuestion->game->creator_id, $this->offlineUser->id])->get();
        } else {
            $gameAnswers = $this->gameQuestion->gameAnswers;
        }

        /**
         * @var GameAnswer $creatorGameAnswer
         */
        $creatorGameAnswer = $this->gameQuestion->gameAnswers()->where('user_id', $this->gameQuestion->game->creator_id)->first();

        $creatorPoints = 0;
        if ($creatorGameAnswer && $creatorGameAnswer->answer->is_right) {
            $creatorPoints = $creatorGameAnswer->answer->question->weight;

            if ($creatorGameAnswer->is_first) {
                ++$creatorPoints;
            }
        }

        if ($this->offlineUser) {
            $opponentGameUser = $this->gameQuestion->game
                ->gameUsers()
                ->where('user_id', $this->offlineUser->id)
                ->where('user_id', '!=', $this->gameQuestion->game->creator_id)
                ->first();
        } else {
            $opponentGameUser = $this->gameQuestion->game
                ->gameUsers()
                ->whereNotNull('started_at')
                ->whereNull('finished_at')
                ->where('user_id', '!=', $this->gameQuestion->game->creator_id)
                ->first();
        }


        $opponentPoints = 0;
        if ($opponentGameUser) {
            /**
             * @var GameAnswer $opponentGameAnswer
             */
            $opponentGameAnswer = $this->gameQuestion->gameAnswers()->where('user_id', $opponentGameUser->user_id)->first();
            if ($opponentGameAnswer && $opponentGameAnswer->answer->is_right) {
                $opponentPoints = $opponentGameAnswer->answer->question->weight;

                if ($opponentGameAnswer->is_first) {
                    ++$opponentPoints;
                }
            }
        }

        $result = [
            'game_answers' => GameAnswerResource::collection($gameAnswers),
            'correct_answer_id' => $this->gameQuestion->question->answers()->where('is_right', true)->first()->id,
            'ttw' => GameService::ANSWER_TO_SHOW,
            'points' => [
                'creator' => $creatorPoints,
                'opponent' => $opponentPoints
            ]
        ];

        if ($this->offlineUser) {
            Websocket::to($this->offlineUser->roomChannel($this->gameQuestion->game_id))->emit('question-closed', $result);
        } else {
            Websocket::to('game_' . $this->gameQuestion->game_id)->emit('question-closed', $result);
        }
    }
}
