<?php

namespace App\Jobs;

use App\Game;
use App\Http\Resources\GameResource;
use App\User;
use Illuminate\Support\Carbon;
use SwooleTW\Http\Websocket\Facades\Websocket;

class SendGameStarted extends Job
{

    protected $game;
    protected $offlineUser;

    /**
     * Create a new job instance.
     *
     * @param Game $game
     * @param User $offlineUser
     */
    public function __construct(Game $game, User $offlineUser = null)
    {
        $this->game = $game;
        $this->offlineUser = $offlineUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->game->is_active = true;
        $this->game->started_at = Carbon::now();
        $this->game->save();

        if ($this->offlineUser) {
            $this->game->gameUsers()->where('user_id', $this->offlineUser->id)->update([
                'started_at' => \Carbon\Carbon::now()
            ]);
        } else {
            $this->game->gameUsers()->update([
                'started_at' => \Carbon\Carbon::now()
            ]);
        }

        if ($this->offlineUser) {
            Websocket::to($this->offlineUser->roomChannel($this->game->id))->emit('game-stared', (new GameResource($this->game))->resolve());
        } else {
            Websocket::to('game_' . $this->game->id)->emit('game-stared', (new GameResource($this->game))->resolve());
        }
    }
}
