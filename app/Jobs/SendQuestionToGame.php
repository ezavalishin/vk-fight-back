<?php

namespace App\Jobs;

use App\GameQuestion;
use App\Http\Resources\QuestionResource;
use App\User;
use Illuminate\Support\Carbon;
use SwooleTW\Http\Websocket\Facades\Websocket;

class SendQuestionToGame extends Job
{
    protected $gameQuestion;
    protected $offlineUser;
    protected $questionOrder;

    public function __construct(GameQuestion $gameQuestion, User $offlineUser = null, int $questionOrder = null)
    {
        $this->gameQuestion = $gameQuestion;
        $this->offlineUser = $offlineUser;
        $this->questionOrder = $questionOrder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->gameQuestion->is_active = true;
        $this->gameQuestion->started_at = Carbon::now();
        $this->gameQuestion->save();

        $this->gameQuestion->question->load(['answers']);


        if ($this->gameQuestion->game->is_online) {
            $this->gameQuestion->game->gameUsers()->update([
                'last_question_at' => Carbon::now()
            ]);
        } else {
            $gameUser = $this->gameQuestion->game->gameUsers()->where('user_id', $this->offlineUser->id)->first();

            if ($gameUser) {
                $gameUser->last_question_at = Carbon::now();
                $gameUser->save();
            }
        }

        if ($this->questionOrder) {
            $this->gameQuestion->question->setAttribute('order', $this->questionOrder);
        }

        if ($this->offlineUser) {
            Websocket::to($this->offlineUser->roomChannel($this->gameQuestion->game_id))->emit('new-question', (new QuestionResource($this->gameQuestion->question))->resolve());
        } else {
            Websocket::to('game_' . $this->gameQuestion->game_id)->emit('new-question', (new QuestionResource($this->gameQuestion->question))->resolve());
        }
    }
}
