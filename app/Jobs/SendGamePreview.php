<?php

namespace App\Jobs;

use App\Game;
use App\Http\Resources\GameResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use SwooleTW\Http\Websocket\Facades\Websocket;

class SendGamePreview extends Job
{
    protected $game;
    protected $offlineUser;

    /**
     * Create a new job instance.
     *
     * @param Game $game
     * @param User $offlineUser
     */
    public function __construct(Game $game, User $offlineUser = null)
    {
        $this->game = $game;
        $this->offlineUser = $offlineUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->game->started_at = Carbon::now();
        $this->game->save();

        $this->game->load(['creator', 'gameUsers.user']);

        $creatorGameUser = $this->game->gameUsers()->where('user_id', $this->game->creator_id)->first();

        if ($this->offlineUser) {
            $opponent = $this->game->gameUsers
                ->where('user_id', $this->offlineUser->id)
                ->where('user_id', '!=', $this->game->creator_id)
                ->first();
        } else {
            $opponent = $this->game->gameUsers->where('user_id', '!=', $this->game->creator_id)->first();
        }


        $points = [
            'creator' => $creatorGameUser->points
        ];

        if ($opponent) {

            $points['opponent'] = $opponent->points;

            $opponent = $opponent->user;
        }

        $this->game->setAttribute('points', $points);
        $this->game->setRelation('opponent', $opponent);

        $this->game->loadCount('gameQuestions');

        if ($this->offlineUser) {
            Websocket::to($this->offlineUser->roomChannel($this->game->id))->emit('game-preview', (new GameResource($this->game))->resolve());
        } else {
            Websocket::to('game_' . $this->game->id)->emit('game-preview', (new GameResource($this->game))->resolve());
        }
    }
}
