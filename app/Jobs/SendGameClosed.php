<?php

namespace App\Jobs;

use App\Game;
use App\GameLog;
use App\Http\Resources\CloseGameResource;
use App\User;
use Illuminate\Support\Carbon;
use SwooleTW\Http\Websocket\Facades\Room;
use SwooleTW\Http\Websocket\Facades\Websocket;

class SendGameClosed extends Job
{

    protected $game;
    protected $offlineUser;

    /**
     * Create a new job instance.
     *
     * @param Game $game
     * @param User $offlineUser
     */
    public function __construct(Game $game, User $offlineUser = null)
    {
        $this->game = $game;
        $this->offlineUser = $offlineUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->game->is_active = false;
        $this->game->finished_at = Carbon::now();
        $this->game->save();

        if ($this->offlineUser) {
            $this->game->gameUsers()->where('user_id', $this->offlineUser->id)->update([
                'finished_at' => Carbon::now()
            ]);
        } else {
            $this->game->gameUsers()->update([
                'finished_at' => Carbon::now()
            ]);
        }


        if ($this->game->is_online) {
            $creatorGameUser = $this->game->gameUsers()->where('user_id', $this->game->creator_id)->first();
            $opponentGameUser = $this->game->gameUsers()->where('user_id', '!=', $this->game->creator_id)->first();

            if ($creatorGameUser->points > $opponentGameUser->points) {
                $this->game->winner_id = $this->game->creator_id;
            } elseif ($creatorGameUser->points < $opponentGameUser->points) {
                $this->game->winner_id = $opponentGameUser->user_id;
            } else {
                $this->game->winner_id = null;
            }

            $this->game->save();
        } else {
            $creatorGameUser = $this->game->gameUsers()->where('user_id', $this->game->creator_id)->first();
            $opponentGameUser = $this->game->gameUsers()
                ->where('user_id', '!=', $this->game->creator_id)
                ->where('user_id', $this->offlineUser->id)
                ->first();

            if (!$opponentGameUser) {
                $this->game->setAttribute('need_share', true);
            } else {
                if ($creatorGameUser->points > $opponentGameUser->points) {
                    $this->game->winner_id = $this->game->creator_id;
                } elseif ($creatorGameUser->points < $opponentGameUser->points) {
                    $this->game->winner_id = $opponentGameUser->user_id;
                } else {
                    $this->game->winner_id = null;
                }
            }
        }

        if ($this->offlineUser) {
            Websocket::to($this->offlineUser->roomChannel($this->game->id))->emit('game-closed', (new CloseGameResource($this->game))->resolve());

            Room::closeAndClearRoom($this->offlineUser->roomChannel($this->game->id));
        } else {
            Websocket::to('game_' . $this->game->id)->emit('game-closed', (new CloseGameResource($this->game))->resolve());

            Room::closeAndClearRoom('game_' . $this->game->id);
        }

        info('rooms: ' . Room::getRoomsCount());
        info('fds: ' . Room::getFdsCount());

        $creatorGameUser->user->updateRating();
        if (!$opponentGameUser) {
            return;
        }

        $opponentGameUser->user->updateRating();

        $userOneId = $this->game->creator_id;
        $userTwoId = $opponentGameUser->user_id;

        GameLog::query()->create([
            'game_id' => $this->game->id,
            'user_one_id' => $userOneId,
            'user_two_id' => $userTwoId,
            'user_one_points' => $creatorGameUser->points,
            'user_two_points' => $opponentGameUser->points
        ]);


    }
}
