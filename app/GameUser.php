<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;


/**
 * App\GameUser
 *
 * @property int $id
 * @property int $user_id
 * @property int $game_id
 * @property int $points
 * @property Carbon|null $started_at
 * @property Carbon|null $finished_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Game $game
 * @property-read User $user
 * @method static Builder|GameUser newModelQuery()
 * @method static Builder|GameUser newQuery()
 * @method static Builder|GameUser query()
 * @method static Builder|GameUser whereCreatedAt($value)
 * @method static Builder|GameUser whereFinishedAt($value)
 * @method static Builder|GameUser whereGameId($value)
 * @method static Builder|GameUser whereId($value)
 * @method static Builder|GameUser wherePoints($value)
 * @method static Builder|GameUser whereStartedAt($value)
 * @method static Builder|GameUser whereUpdatedAt($value)
 * @method static Builder|GameUser whereUserId($value)
 * @mixin Eloquent
 * @property Carbon|null $last_question_at
 * @method static Builder|GameUser whereLastQuestionAt($value)
 */
class GameUser extends Model
{
    protected $fillable = [
        'game_id',
        'user_id',
        'points',
        'started_at',
        'finished_at'
    ];

    protected $dates = [
        'started_at',
        'finished_at',
        'last_question_at',
    ];

    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
