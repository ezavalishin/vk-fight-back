# Игра

Url для сокетов: `wss://vk-battle.ezavalishin.ru`

```js
socket = io(url, {
   transports: ['websocket'],
});
```

## REST

### Авторизация

`POST /vk-user/auth`

```json
{
  "data": {
    "id": 13162803,
    "first_name": "Евгений",
    "last_name": "Завалишин",
    "avatar": "https:\/\/sun9-63.userapi.com\/c845520\/v845520685\/378a\/RBMu__rIwt4.jpg?ava=1",
    "rating": 16,
    "games_count": 2,
    "wins_count": 1,
    "friends": [
      {
        "id": 255424239,
        "first_name": "Элеонора",
        "last_name": "Мухамедшина",
        "avatar": "https:\/\/sun9-32.userapi.com\/c857736\/v857736725\/85565\/RH14oF2vTFc.jpg?ava=1",
        "rating": 0
      }
    ]
  }
}
```

### Рейтинг

Обшиий `GET /vk-user/rating`

Друзья `GET /vk-user/rating?friends=1`

```json
{
  "data": [
    {
      "id": 13162803,
      "first_name": "Евгений",
      "last_name": "Завалишин",
      "avatar": "https:\/\/sun9-63.userapi.com\/c845520\/v845520685\/378a\/RBMu__rIwt4.jpg?ava=1",
      "rating": 16
    },
    {
      "id": 255424239,
      "first_name": "Элеонора",
      "last_name": "Мухамедшина",
      "avatar": "https:\/\/sun9-32.userapi.com\/c857736\/v857736725\/85565\/RH14oF2vTFc.jpg?ava=1",
      "rating": 0
    },
    {
      "id": 89009215,
      "first_name": "Иван",
      "last_name": "Кондрахин",
      "avatar": "https:\/\/sun9-41.userapi.com\/c846320\/v846320710\/f204e\/933zmQw--9w.jpg?ava=1",
      "rating": 0
    }
  ],
  "links": {
    "first": "http:\/\/vk-fight-back.test\/vk-user\/rating?page=1",
    "last": "http:\/\/vk-fight-back.test\/vk-user\/rating?page=1",
    "prev": null,
    "next": null
  },
  "meta": {
    "current_page": 1,
    "from": 1,
    "last_page": 1,
    "path": "http:\/\/vk-fight-back.test\/vk-user\/rating",
    "per_page": 15,
    "to": 3,
    "total": 3
  }
}
```


### Мои игры

`GET /vk-user/my-games`

```json
{
  "data": [
    {
      "id": 2,
      "creator": {
        "points": 5,
        "user": {
          "id": 13162803,
          "first_name": "Евгений",
          "last_name": "Завалишин",
          "avatar": "https:\/\/sun9-63.userapi.com\/c845520\/v845520685\/378a\/RBMu__rIwt4.jpg?ava=1",
          "rating": 16
        }
      },
      "opponent": {
        "points": 4,
        "user": {
          "id": 89009215,
          "first_name": "Иван",
          "last_name": "Кондрахин",
          "avatar": "https:\/\/sun9-41.userapi.com\/c846320\/v846320710\/f204e\/933zmQw--9w.jpg?ava=1",
          "rating": 0
        }
      },
      "created_at": "2020-04-11 16:02:40"
    },
    {
      "id": 1,
      "creator": {
        "points": 5,
        "user": {
          "id": 13162803,
          "first_name": "Евгений",
          "last_name": "Завалишин",
          "avatar": "https:\/\/sun9-63.userapi.com\/c845520\/v845520685\/378a\/RBMu__rIwt4.jpg?ava=1",
          "rating": 16
        }
      },
      "opponent": {
        "points": 6,
        "user": {
          "id": 255424239,
          "first_name": "Элеонора",
          "last_name": "Мухамедшина",
          "avatar": "https:\/\/sun9-32.userapi.com\/c857736\/v857736725\/85565\/RH14oF2vTFc.jpg?ava=1",
          "rating": 0
        }
      },
      "created_at": "2020-04-11 15:59:59"
    }
  ],
  "links": {
    "first": "http:\/\/vk-fight-back.test\/vk-user\/my-games?page=1",
    "last": "http:\/\/vk-fight-back.test\/vk-user\/my-games?page=1",
    "prev": null,
    "next": null
  },
  "meta": {
    "current_page": 1,
    "from": 1,
    "last_page": 1,
    "path": "http:\/\/vk-fight-back.test\/vk-user\/my-games",
    "per_page": 15,
    "to": 2,
    "total": 2
  }
}
```

## Процесс игры


### Ошибки

#### 😀 <- error

При возникновеннии ошибки, приходит событие `error`, со следующей структурой:

```json
{
  "message": "$message",
  "code": "$code"
}
```


#### Коды ошибок

| Код | Описание | Когда возникает |
|---|---|---|
| 1 | Игра уже началась | `connect-to-online-game` |
| 2 | Игра уже завершилась | `connect-to-online-game` |
| 3 | Игра уже сыграна | `connect-to-online-game` |
| 4 | Пользователь офлайн | `send-invite` |
| 5 | Игра не найдена | `connect-to-online-game`, `send-invite`, `send-invite-by-game`, `get-active-question` |
| 6 | Игрок не найден | `send-invite` |
| 7 | Игра с самим собой | `connect-to-online-game` |
| 8 | Создатель уже начал игру | `connect-to-online-game` |

### Онлайн

#### 😀 -> create-online-game

1. Пользоваьель создает игру событием `create-online-game`


#### 😀 <- game-created

2. Получает событие `game-created`, в котором будет id игры
```json
{
  "id": 5,
  "uuid": "dbb12fee-5e83-4bdb-9cc2-0ffe7bc0b993",
  "ttw": 3,
  "winner_id": null
}
```


#### 😀 -> connect-to-online-game

3. Другой пользователь может подключиться через `connect-to-online-game`, передав
```json
{
  "game_id": this.gameId, // одно из
  "game_uuid": this.gameUuid // одно из
}
``` 


#### 😀 <- game-preview

4. Как только все подключились, приходит событие `game-preview`, `ttw` обозначает время до следующего события

```json
{
  "id":5,
  "ttw":3,
  "questions_count": 7,
  "creator":{
    "id":13162803,
    "first_name":"\u0415\u0432\u0433\u0435\u043d\u0438\u0439",
    "last_name":"\u0417\u0430\u0432\u0430\u043b\u0438\u0448\u0438\u043d",
    "avatar":"https:\/\/sun9-63.userapi.com\/c845520\/v845520685\/378a\/RBMu__rIwt4.jpg?ava=1"
  },
  "opponent":{
    "id":13162803,
    "first_name":"\u0415\u0432\u0433\u0435\u043d\u0438\u0439",
    "last_name":"\u0417\u0430\u0432\u0430\u043b\u0438\u0448\u0438\u043d",
    "avatar":"https:\/\/sun9-63.userapi.com\/c845520\/v845520685\/378a\/RBMu__rIwt4.jpg?ava=1"
  },
  "points": {
    "creator": 0,
    "opponent": 0
  },
  "winner_id":null
}
```


#### 😀 <- game-started

5. Через `ttw` придет событие `game-stared`, игра началась


#### 😀 <- new-question


6. Сразу же за ним событие `new-question`

```json
{
  "id":26,
  "order": 1,
  "title":"\u041a\u0442\u043e \u0438\u0437 \u044d\u0442\u0438\u0445 \u043b\u044e\u0434\u0435\u0439 \u0443\u043f\u0440\u0430\u0432\u043b\u044f\u0435\u0442 \u0447\u0430\u0441\u0442\u043d\u043e\u0439 \u043a\u043e\u0441\u043c\u0438\u0447\u0435\u0441\u043a\u043e\u0439 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0435\u0439?",
  "answers":[
    {
      "id":76,
      "title":"\u0418\u043b\u043e\u043d \u041c\u0430\u0441\u043a"
    },
    {
    "id":77,
    "title":"\u0421\u0435\u0440\u0433\u0435\u0439 \u0411\u0440\u0438\u043d"
    },
    {
    "id":78,
    "title":"\u0414\u0436\u0435\u0439\u043c\u0441 \u041a\u044d\u043c\u0435\u0440\u043e\u043d"
    }
  ],
  "ttw":7
}
```


#### 😀 <- question-closed

7. Через `ttw` событие `question-closed`, в котором содержится верный ответ, ответы игроков, и их очки за данный вопрос

```json
{
  "game_answers": [
    {
      "id":50,
      "answer_id":49,
      "user": {
        "id":13162803,
        "first_name":"\u0415\u0432\u0433\u0435\u043d\u0438\u0439",
        "last_name":"\u0417\u0430\u0432\u0430\u043b\u0438\u0448\u0438\u043d",
        "avatar":"https:\/\/sun9-63.userapi.com\/c845520\/v845520685\/378a\/RBMu__rIwt4.jpg?ava=1"
      }
    }
  ],
  "correct_answer_id":49,
  "ttw":3,
  "points":{
    "creator":2,
    "opponent":0
  }
}
```


#### 😀 <- game-closed

8. Через череду вопросов и ответов приходит `game-closed`, в котором содержится id победителя
 
```json
{
  "id": 7,
  "ttw": 3,
  "winner_id": 13162803
}
```


#### 😀 <- badge

Приходит просто чотбы показать всплывашку `badge`

```json
{
  "message": "\u041f\u0435\u0440\u0432\u044b\u0439!"
}
```


### Приглашения

#### 😀 -> send-invite

Для приглашения в игру необхоимо отправить событие `send-invite` с параметрами
```json
{
  "user_id": 1, // id пользователя для приглашения
  "game_id": 1 // id игры полученный с помощью create-online-game
}
```


#### 😀 -> send-invite-by-game

Для приглашения в игру необхоимо отправить событие `send-invite-by-game` с параметрами
```json
{
  "last_game_id": 1, // id прошлой игры
  "game_id": 1 // id игры полученный с помощью create-online-game
}
```


#### 😀 <- game-invite

Приглашенному пользователю приходит событие `game-invite`, для вступление нужно так же вызвать `connect-to-online-game`

Параметры `game-invite`:

```json
{
  "game_id":1,
  "creator":{
    "id":13162803,
    "first_name":"\u0415\u0432\u0433\u0435\u043d\u0438\u0439",
    "last_name":"\u0417\u0430\u0432\u0430\u043b\u0438\u0448\u0438\u043d",
    "avatar":"https:\/\/sun9-63.userapi.com\/c845520\/v845520685\/378a\/RBMu__rIwt4.jpg?ava=1",
    "rating":16
  }
}
```


#### 😀 -> decline-invite

Для отклонения приглашения нужно отправить событие `decline-invite`

Параметры `decline-invite`:

```json
{
  "game_id":1 // игра, в которую прислано приглашение
}
```


#### 😀 <- invite-declined

Данное событие приходит, когда пользователь отклонил игру


### Подключение в активную игру

#### 😀 -> get-active-game

Сначала необходимо получить id активной игры. Для этого необхоимо отправить `get-active-game`, в ответ придет `active-game`


#### 😀 <- active-game

С параметрами:

```json
{
  "game_id": 1, // активная игра, либо null - если активной игры нет
  "is_online": true // либо false
}
```

Для подключения к этой игре нужно так же вызвать `connect-to-online-game`, далее по флоу процесса игры


#### 😀 -> get-active-question

Получить активный вопрос. Вызывать после коннекта к игре. Возвращает ошибку, если нет активной игры.

#### 😀 <- active-question

Возвращает, если сейчас активного вопроса нет, следует дождаться следующий ивент: 

```json
{
  "id": null
}
```

Если есть вопрос:

```json
{
  "id":26,
  "order": 1,
  "title":"\u041a\u0442\u043e \u0438\u0437 \u044d\u0442\u0438\u0445 \u043b\u044e\u0434\u0435\u0439 \u0443\u043f\u0440\u0430\u0432\u043b\u044f\u0435\u0442 \u0447\u0430\u0441\u0442\u043d\u043e\u0439 \u043a\u043e\u0441\u043c\u0438\u0447\u0435\u0441\u043a\u043e\u0439 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0435\u0439?",
  "answers":[
    {
      "id":76,
      "title":"\u0418\u043b\u043e\u043d \u041c\u0430\u0441\u043a"
    },
    {
    "id":77,
    "title":"\u0421\u0435\u0440\u0433\u0435\u0439 \u0411\u0440\u0438\u043d"
    },
    {
    "id":78,
    "title":"\u0414\u0436\u0435\u0439\u043c\u0441 \u041a\u044d\u043c\u0435\u0440\u043e\u043d"
    }
  ],
  "ttw":7
}
```


